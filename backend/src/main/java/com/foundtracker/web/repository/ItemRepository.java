package com.foundtracker.web.repository;

import com.foundtracker.web.enums.ReclamationStatus;
import com.foundtracker.web.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;


public interface ItemRepository extends JpaRepository<Item, Long>, JpaSpecificationExecutor<Item> {

    // Method to get the count of all items
    long count();
    @Query("select count(e) from Reclamation e where e.status = ?1")
    long countByStatus(ReclamationStatus status);

    @Query("SELECT MONTH(i.createdAt) AS month, COUNT(i) AS count " +
        "FROM Item i " +
        "WHERE YEAR(i.createdAt) = :year " +
        "GROUP BY MONTH(i.createdAt)")
    List<Map<String, Object>> findCountsByMonth(@Param("year") int year);
}
