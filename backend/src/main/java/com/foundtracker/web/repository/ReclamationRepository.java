package com.foundtracker.web.repository;

import com.foundtracker.web.enums.ReclamationStatus;
import com.foundtracker.web.model.Reclamation;
import com.foundtracker.web.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ReclamationRepository extends JpaRepository<Reclamation, Long>, JpaSpecificationExecutor<Reclamation> {
    Page<Reclamation> findAllByUser(User user, Pageable pageable);


    Optional<Reclamation> findByUserAndId(User user, Long id);

    Optional<Reclamation> findByUserAndCode(User user, String code);

    Optional<Reclamation> findByCode(String code);

    @Query("select count(e) from Reclamation e where e.status = ?1")
    long countByStatus(ReclamationStatus status);

    @Query("SELECT MONTH(i.createdAt) AS month, COUNT(i) AS count " +
        "FROM Reclamation i " +
        "WHERE YEAR(i.createdAt) = :year " +
        "GROUP BY MONTH(i.createdAt)")
    List<Map<String, Object>> findCountsByMonth(@Param("year") int year);



}
