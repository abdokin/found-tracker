package com.foundtracker.web.dto;

import com.foundtracker.web.model.Item;
import com.foundtracker.web.validator.ValidImageFiles;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Data
@Schema(name = "CreateItem")
public class CreateItem {
    @NotNull(message = "Name cannot be null")
    @NotBlank(message = "Name cannot be empty")
    @Size(min = 1, max = 255, message = "Name length must be between 1 and 255 characters")
    private String name;

    @NotNull(message = "Description cannot be null")
    @NotBlank(message = "Description cannot be empty")
    @Size(max = 1000, message = "Description length cannot exceed 1000 characters")
    private String description;

    @NotNull(message = "Images cannot be null")
    @Size(min = 1, max = 5, message = "Images length must be between 1 and 5")
    @ValidImageFiles
    List<MultipartFile> images;

    public static Item mapToDto(CreateItem CreateItem) {
        return Item.builder()
                .name(CreateItem.name)
                .description(CreateItem.description)
                .foundDateTime(LocalDateTime.now())
                .build();
    }
}
