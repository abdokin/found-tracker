package com.foundtracker.web.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.foundtracker.web.model.Role;
import com.foundtracker.web.model.User;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "User")
public class UserDto {

    private Integer id;

    private String firstname;
    private String lastname;
    private String email;
    private String cne;
    private String address;
    private String phone;
    private boolean enabled;
    private LocalDate dateBirth;
    private Role role;
    private LocalDateTime createdAt;
    private List<NotificationDto> notifications;

    public static UserDto mapToUserDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .email(user.getEmail())
                .createdAt(user.getCreatedAt())
                .role(user.getRole())
                .cne(user.getCne())
                .address(user.getAddress())
                .enabled(user.getEnabled())
                .phone(user.getPhone())
                .notifications(user.getNotifications() != null ?user.getNotifications().stream().map(NotificationDto::mapToDto).toList(): null)
                .dateBirth(user.getDateBirth())
                .build();
    }
    

}
