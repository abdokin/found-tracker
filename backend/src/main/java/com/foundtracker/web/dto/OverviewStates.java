package com.foundtracker.web.dto;

import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
@Data
@Builder
@AllArgsConstructor
public class OverviewStates {
    long totalItems;
    long totalReclamations;
    long totalUsers;
    List<Map<String, Object>> itemMonthlyCount;
    List<Map<String, Object>> reclamationMonthlyCount;

}
