package com.foundtracker.web.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import com.foundtracker.web.validator.ValidImageFiles;

import java.util.List;

@Builder
@Data
public class CreateReclamation {
    @NotNull(message = "Sujet cannot be null")
    @NotBlank(message = "Sujet cannot be empty")
    @Size(min = 1, max = 255, message = "Sujet length must be between 1 and 255 characters")
    private String sujet;

    @NotNull(message = "Item id cannot be null")
    private Long itemId;

    @NotNull(message = "Description cannot be null")
    @NotBlank(message = "Description cannot be empty")
    @Size(max = 1000, message = "Description length cannot exceed 1000 characters")
    private String description;

    @NotNull(message = "Images cannot be null")
    @Size(min = 1, max = 5, message = "Images lenght must be between 1 and 5")
    @ValidImageFiles
    List<MultipartFile> images;

}
