package com.foundtracker.web.enums;

import lombok.Getter;

@Getter
public enum ReclamationStatus {
    PENDING("PENDING"),
    ACCEPTED("ACCEPTED"),
    REJECTED("REJECTED");


    private final String status;

     ReclamationStatus(String status) {
        this.status = status;
    }
}
