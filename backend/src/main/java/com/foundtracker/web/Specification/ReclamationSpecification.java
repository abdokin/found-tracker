package com.foundtracker.web.Specification;

import java.time.LocalDateTime;

import org.springframework.data.jpa.domain.Specification;

import com.foundtracker.web.enums.ReclamationStatus;
import com.foundtracker.web.model.Reclamation;

import jakarta.persistence.criteria.Predicate;

public class ReclamationSpecification {
    public static Specification<Reclamation> sujetLike(String sujet) {
        return (root, query, builder) -> builder.like(root.get("sujet"), "%" + sujet + "%");
    }

    public static Specification<Reclamation> status(ReclamationStatus[] itemStatus) {
        return (root, query, builder) -> {
            Predicate statusPredicate = root.get("status").in((Object[]) itemStatus);
            return statusPredicate;
        };
    }

    public static Specification<Reclamation> createdAt(LocalDateTime createdAt) {
        return (root, query, builder) -> builder.greaterThan(root.get("createdAt"), createdAt);
    }

}
