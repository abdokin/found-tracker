package com.foundtracker.web.Specification;
import org.springframework.data.jpa.domain.Specification;

import com.foundtracker.web.model.User;

public class UserSpecification {
    public static Specification<User> nameLike(String sujet) {
        return (root, query, builder) -> builder.like(root.get("firstname"), "%" + sujet + "%");
    }

    public static Specification<User> status(boolean enabled) {
        return (root, query, builder) -> builder.equal(root.get("enabled"), enabled);
    }


}
