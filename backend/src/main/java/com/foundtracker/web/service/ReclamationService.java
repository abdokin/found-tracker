package com.foundtracker.web.service;

import com.foundtracker.web.Specification.ReclamationSpecification;
import com.foundtracker.web.dto.CreateReclamation;
import com.foundtracker.web.dto.ReclamationDto;
import com.foundtracker.web.enums.ItemStatus;
import com.foundtracker.web.enums.ReclamationStatus;
import com.foundtracker.web.model.Image;
import com.foundtracker.web.model.Item;
import com.foundtracker.web.model.Reclamation;
import com.foundtracker.web.model.Role;
import com.foundtracker.web.model.User;
import com.foundtracker.web.repository.ItemRepository;
import com.foundtracker.web.repository.ReclamationRepository;

import io.micrometer.common.util.StringUtils;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReclamationService {
    private final ReclamationRepository reclamationRepository;
    private final ItemRepository itemRepository;
    private final NotificationService notificationService;
    private final ImageService imageService;
    private final UserService userService;
    private Random random = new Random();

    public void save(CreateReclamation input) throws IOException {
        Item item = itemRepository.findById(input.getItemId()).orElseThrow();
        Reclamation reclamation = reclamationRepository.save(Reclamation.builder()
                .item(item)
                .code(generateUniqueCode())
                .user(userService.getCurrentUser())
                .sujet(input.getSujet())
                .description(input.getDescription())
                .build());

        List<Image> images = imageService.storeImages(input.getImages(), reclamation);

        reclamation.setImages(images);
        notificationService.send(
                "your reclamation has been created with key " + reclamation.getCode(),
                "Reclamation Created", reclamation);
    }

    private Reclamation findByIdAndCurrent(long reclamationId) {
        User current = userService.getCurrentUser();
        if (current != null && current.getRole().equals(Role.RECEPTIONNAIRE)) {
            return reclamationRepository.findById(reclamationId).orElseThrow();
        }
        return reclamationRepository.findByUserAndId(current, reclamationId).orElseThrow();
    }

    public ReclamationDto findById(long reclamationId) {
        return ReclamationDto.mapToDto(this.findByIdAndCurrent(reclamationId));
    }

    public Page<ReclamationDto> findAll(Pageable pageable, String sujet, ReclamationStatus[] status,
            LocalDateTime date) {
        Specification<Reclamation> filters = Specification
                .where(StringUtils.isBlank(sujet) ? null : ReclamationSpecification.sujetLike(sujet))
                .and(status == null ? null : ReclamationSpecification.status(status))
                .and(date == null ? null : ReclamationSpecification.createdAt(date));
        Page<Reclamation> reclmatations = reclamationRepository.findAll(filters, pageable);
        return reclmatations.<ReclamationDto>map(ReclamationDto::mapToDto);
    }

    public ReclamationDto reject(Long reclamationId) {
        Reclamation reclamation = reclamationRepository.findById(reclamationId).orElseThrow();
        if (!reclamation.getStatus().equals(ReclamationStatus.PENDING))
            throw new NoSuchElementException();
        reclamation.setStatus(ReclamationStatus.REJECTED);
        notificationService.send("your reclamation has been rejected", "Reclamation Reject",
                reclamationRepository.save(reclamation));

        return ReclamationDto.mapToDto(reclamation);
    }

    public ReclamationDto accept(Long reclamationId) {
        Reclamation reclamation = reclamationRepository.findById(reclamationId).orElseThrow();
        if (!reclamation.getStatus().equals(ReclamationStatus.PENDING))
            throw new NoSuchElementException();

        reclamation.setStatus(ReclamationStatus.ACCEPTED);
        reclamationRepository.save(reclamation);
        notificationService.send(
                "your reclamation has been accepted with key " + reclamation.getCode() + " to claim your objet",
                "Reclamation Accepted",
                reclamation);

        return ReclamationDto.mapToDto(reclamation);
    }

    public String generateUniqueCode() {
        LocalDateTime now = LocalDateTime.now();
        String datePart = now.toString().replace("-", "").replace(":", "").replace(".", "");
        String randomPart = String.format("%03d", random.nextInt(1000)); // Random 3-digit number
        return datePart + randomPart;
    }

    public ReclamationDto claim(String reclamationCode) {

        User current = userService.getCurrentUser();
        Reclamation reclamation = null;
        if (current.getRole().equals(Role.RECEPTIONNAIRE)) {
            reclamation = reclamationRepository.findByCode(reclamationCode).orElseThrow();
        } else {
            reclamation = reclamationRepository.findByUserAndCode(current, reclamationCode).orElseThrow();
        }
        if (!reclamation.getStatus().equals(ReclamationStatus.ACCEPTED)) {
            throw new NoSuchElementException();

        }
        reclamation.getItem().setStatus(ItemStatus.CLAIMED);
        return ReclamationDto.mapToDto(reclamationRepository.save(reclamation));
    }

    public ReclamationDto findByCode(String reclamationCode) {
        User current = userService.getCurrentUser();
        Reclamation reclamation = null;
        if (current != null && current.getRole().equals(Role.RECEPTIONNAIRE)) {
            reclamation = reclamationRepository.findByCode(reclamationCode).orElseThrow();
        } else {
            reclamation = reclamationRepository.findByUserAndCode(current, reclamationCode).orElseThrow();
        }
        return ReclamationDto.mapToDto(reclamation);
    }

}
