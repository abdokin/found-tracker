package com.foundtracker.web.service;

import com.foundtracker.web.dto.OverviewStates;
import com.foundtracker.web.repository.ItemRepository;
import com.foundtracker.web.repository.ReclamationRepository;
import com.foundtracker.web.repository.UserRepository;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;



import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OverviewService {
    private  final ItemRepository itemRepository;
    private final ReclamationRepository reclamationRepository;
    private final UserRepository userRepository;
    public List<Map<String, Object>> countByMonth(List<Map<String, Object>> counts) {

        // Create a map with default counts for each month
        Map<Integer, Map<String, Object>> monthMap = new HashMap<>();
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        
        for (int i = 1; i <= 12; i++) {
            Map<String, Object> monthData = new HashMap<>();
            monthData.put("month", monthNames[i - 1]);
            monthData.put("count", 0);
            monthMap.put(i, monthData);
        }

        // Update the map with actual counts
        for (Map<String, Object> countData : counts) {
            Integer month = (Integer) countData.get("month");
            Long count = (Long) countData.get("count");
            monthMap.get(month).put("count", count.intValue());
        }

        // Collect the results into a list
        List<Map<String, Object>> result = new ArrayList<>(monthMap.values());
        return result;
    }
    public OverviewStates getOverviewStates(int year) {

        return OverviewStates.builder()
                .itemMonthlyCount(this.countByMonth(itemRepository.findCountsByMonth(year)))
                .totalReclamations(reclamationRepository.count())
                .totalUsers(userRepository.count())
                .totalItems(itemRepository.count())
                .reclamationMonthlyCount(this.countByMonth(reclamationRepository.findCountsByMonth(year)))
                .build();
    }
}
