package com.foundtracker.web.service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.foundtracker.web.model.Image;
import com.foundtracker.web.model.Item;
import com.foundtracker.web.model.Reclamation;
import com.foundtracker.web.repository.ImageRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ImageService {
    private final ImageRepository imageRepository;
    private final StorageService storageService;

    public List<Image> storeImages(List<MultipartFile> files, Item item) throws java.io.IOException {
        List<Image> images = files.stream()
                .map(file -> {
                    try {
                        String url = storageService.storeFile(file);
                        String name = file.getOriginalFilename();
                        return Image.builder()
                                .item(item)
                                .imageName(name)
                                .imageUrl(url)
                                .build();
                    } catch (IOException e) {
                        throw new RuntimeException("Error storing file: " + e.getMessage());
                    }
                })
                .collect(Collectors.toList());

        imageRepository.saveAll(images);
        return images.stream()
                .peek(it -> it.setItem(null))
                .toList();
    }

    public List<Image> storeImages(List<MultipartFile> files, Reclamation reclamation) throws java.io.IOException {
        List<Image> images = files.stream()
                .map(file -> {
                    try {
                        String url = storageService.storeFile(file);
                        String name = file.getOriginalFilename();
                        return Image.builder()
                                .reclamation(reclamation)
                                .imageName(name)
                                .imageUrl(url)
                                .build();
                    } catch (IOException e) {
                        throw new RuntimeException("Error storing file: " + e.getMessage());
                    }
                })
                .collect(Collectors.toList());

        imageRepository.saveAll(images);
        return images.stream()
                .peek(it -> it.setItem(null))
                .toList();
    }

    public void deleteImage(Image image) {
        storageService.deleteFile(image.getImageUrl());
        imageRepository.delete(image);
    }
}
