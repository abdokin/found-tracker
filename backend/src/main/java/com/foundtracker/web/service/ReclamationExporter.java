package com.foundtracker.web.service;

import com.foundtracker.web.dto.ReclamationDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class ReclamationExporter {

    public byte[] generatePDF(ReclamationDto reclamation) throws IOException {
        // Load HTML template
        ClassPathResource resource = new ClassPathResource("pdf/index.html");
        String htmlContent = new String(Files.readAllBytes(resource.getFile().toPath()), StandardCharsets.UTF_8);
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        htmlContent = htmlContent.replace("{{code}}", reclamation.getCode())
                .replace("{{date}}", LocalDate.now().format(dateFormatter))
                .replace("{{subject}}", reclamation.getSujet())
                .replace("{{rDescription}}", reclamation.getDescription())
                .replace("{{name}}", reclamation.getItem().getName())
                .replace("{{iDescription}}", reclamation.getItem().getDescription())
                .replace("{{foundDateTime}}", reclamation.getItem().getFoundDateTime().toString())
                .replace("{{cne}}", reclamation.getUser().getCne())
                .replace("{{firstName}}", reclamation.getUser().getFirstname())
                .replace("{{lastName}}", reclamation.getUser().getLastname())
                .replace("{{email}}", reclamation.getUser().getEmail())
                .replace("{{phone}}", reclamation.getUser().getPhone())
                .replace("{{address}}", reclamation.getUser().getAddress());

        // Convert the HTML content to a PDF
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(htmlContent);
            renderer.layout();
            renderer.createPDF(outputStream);
            return outputStream.toByteArray();
        } catch (Exception e) {
            throw new IOException("Failed to generate PDF", e);
        }
    }
}
