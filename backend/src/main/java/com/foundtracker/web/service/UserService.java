package com.foundtracker.web.service;

import com.foundtracker.web.Specification.UserSpecification;
import com.foundtracker.web.dto.ChangePassword;
import com.foundtracker.web.dto.EditProfile;
import com.foundtracker.web.dto.UserDto;
import com.foundtracker.web.exception.FieldsNotMatch;
import com.foundtracker.web.exception.IncorrectPasswordException;
import com.foundtracker.web.model.User;
import com.foundtracker.web.repository.UserRepository;

import io.micrometer.common.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    public Page<UserDto> getAllUsers(Pageable pageable, String status, String name) {
        
            Specification<User> filters = Specification
                    .where(StringUtils.isBlank(name) ? null : UserSpecification.nameLike(name))
                    .and(status == null ? null : UserSpecification.status(Boolean.parseBoolean(status)));
        Page<User> users = userRepository.findAll(filters,pageable);
        return users.map(UserDto::mapToUserDto);
    }

    public void changePassword(ChangePassword request) throws FieldsNotMatch, IncorrectPasswordException {
        var user = getCurrentUser();
        if (!request.getNewPassword().equals(request.getConfirmationPassword()))
            throw new FieldsNotMatch("newPassword", "confirmationPassword");
        if (!passwordEncoder.matches(request.getCurrentPassword(), user.getPassword()))
            throw new IncorrectPasswordException();
        user.setPassword(passwordEncoder.encode(request.getNewPassword()));
        userRepository.save(user);
    }

    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()
                && authentication.getPrincipal() instanceof User) {
            return (User) authentication.getPrincipal();
        } else {
            // If no user is authenticated, or the principal is not a User object
            // You can return null or throw an exception, depending on your application
            // logic
            // Here, I'll return null
            return null;
        }
    }

    public UserDto editProfile(EditProfile input) {
        User user = userRepository.findById(getCurrentUser().getId()).orElseThrow();
        user.setEmail(input.getEmail());
        user.setFirstname(input.getFirstname());
        user.setLastname(input.getLastname());
        user.setPhone(input.getPhone());
        user.setAddress(input.getAddress());
        userRepository.save(user);
        updateAuthentication(user);
        return UserDto.mapToUserDto(user);
    }

    public void enableUser(int userId,boolean status) {
        User user = userRepository.findById(userId).orElseThrow();
        user.setEnabled(status);
        userRepository.save(user);
    }
    
    private void updateAuthentication(User updatedUser) {
        Authentication updatedAuthentication = new UsernamePasswordAuthenticationToken(updatedUser,
                updatedUser.getPassword(), ((UserDetails) updatedUser).getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(updatedAuthentication);
    }



}
