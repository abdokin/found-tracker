package com.foundtracker.web.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(name = "ApiResponse")
public class ApiResponse<T> {
    private boolean success;
    private int status;
    private String message;
    private LocalDateTime timestamp = LocalDateTime.now();
    private T data;
    private java.util.Map<String, String> errors;

    public ApiResponse(boolean success, int status, String message, T data, java.util.Map<String, String> errors) {
        this.success = success;
        this.status = status;
        this.message = message;
        this.data = data;
        this.errors = errors;
    }

    // addValidationError method
    public void addValidationError(String fieldName, String message) {
        if (errors == null) {
            errors = new java.util.HashMap<>();
        }
        errors.put(fieldName, message);
    }

    public static <T> ResponseEntity<ApiResponse<T>> success(T data, String message) {
        ApiResponse<T> response = new ApiResponse<>(true, HttpStatus.OK.value(), message, data, null);
        return ResponseEntity.ok(response);
    }

    public static <T> ResponseEntity<ApiResponse<T>> error(String message, HttpStatus status) {
        return error(message, status, null);
    }

    public static <T> ResponseEntity<ApiResponse<T>> error(String message, HttpStatus status,
            java.util.Map<String, String> errors) {
        ApiResponse<T> response = new ApiResponse<>(false, status.value(), message, null, errors);
        return ResponseEntity.status(status).body(response);
    }

}
