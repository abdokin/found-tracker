package com.foundtracker.web.controller;

import com.foundtracker.web.dto.UserDto;
import com.foundtracker.web.responses.ApiResponse;
import com.foundtracker.web.service.UserService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/api/v1/management/users")
@RequiredArgsConstructor
@Tag(name = "User Management")
public class UserController {
    final UserService userService;

    @GetMapping
    @PreAuthorize("hasRole('ROLE_RECEPTIONNAIRE')")
    @Operation(summary = "Get all users", description = "Retrieve all users.")
    public ResponseEntity<ApiResponse<Page<UserDto>>> getAllUsers(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(required = false) String status,
            @RequestParam(required = false) String name
            ) {
        Page<UserDto> users = userService.getAllUsers(PageRequest.of(page, size), status,name);
        return ApiResponse.success(users, "users loaded successfully");
    }

    @PostMapping("/{userId}/active")
    @PreAuthorize("hasRole('ROLE_RECEPTIONNAIRE')")
    @Operation(summary = "Active user", description = "Enable or disable a user.")
    public ResponseEntity<?> activeUser(@PathVariable int userId) {
        userService.enableUser(userId, true);
        return ApiResponse.success(null, "user activated successfully");
    }

    @PostMapping("/{userId}/block")
    @PreAuthorize("hasRole('ROLE_RECEPTIONNAIRE')")
    @Operation(summary = "Block user", description = "Enable or disable a user.")
    public ResponseEntity<?> blockUser(@PathVariable int userId) {
        userService.enableUser(userId, false);
        return ApiResponse.success(null, "user blocked successfully");
    }

}
