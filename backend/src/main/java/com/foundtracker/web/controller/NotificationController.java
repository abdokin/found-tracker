package com.foundtracker.web.controller;

import com.foundtracker.web.dto.NotificationDto;
import com.foundtracker.web.responses.ApiResponse;
import com.foundtracker.web.service.NotificationService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/api/v1/notification")
@RequiredArgsConstructor
@Tag(name = "Notifications")
public class NotificationController {

    private final NotificationService notificationService;

    @GetMapping
    @Operation(summary = "Get all notifications", description = "Retrieve all notifications.")
    public ResponseEntity<ApiResponse<Page<NotificationDto>>> getAllNotifications(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        var notifications = notificationService
                .getCurrentUserNotification(PageRequest.of(page, size));
        return ApiResponse.success(notifications, "Notifications loaded succefully");
    }
    @PostMapping("/{notificationId}")
    @Operation(summary = "Read notification", description = "Mark a notification as read.")
    public ResponseEntity<ApiResponse<NotificationDto>> readNotification(@PathVariable int notificationId) {
        return ApiResponse.success(notificationService.read(notificationId), "Notification read successfully");
    }

}
