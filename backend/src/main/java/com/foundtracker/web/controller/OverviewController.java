package com.foundtracker.web.controller;

import com.foundtracker.web.dto.OverviewStates;
import com.foundtracker.web.responses.ApiResponse;
import com.foundtracker.web.service.OverviewService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/overview")
@RequiredArgsConstructor
@Tag(name = "Overview")
public class OverviewController {
    private final OverviewService overviewService;

    @GetMapping("monthly/{year}")
    @Operation(summary = "Get monthly overview", description = "Get monthly overview for a given year.")
    @PreAuthorize("hasRole('ROLE_RECEPTIONNAIRE')")
    public ResponseEntity<ApiResponse<OverviewStates>> getMonthlyOverview(@PathVariable final int year) {
        return ApiResponse.success(overviewService.getOverviewStates(year), "Overview loaded successfully");
    }

}
