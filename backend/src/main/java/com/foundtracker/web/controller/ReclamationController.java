package com.foundtracker.web.controller;

import java.io.IOException;
import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.foundtracker.web.dto.CreateReclamation;
import com.foundtracker.web.dto.ItemDto;
import com.foundtracker.web.dto.ReclamationDto;
import com.foundtracker.web.enums.ReclamationStatus;
import com.foundtracker.web.responses.ApiResponse;
import com.foundtracker.web.service.ReclamationExporter;
import com.foundtracker.web.service.ReclamationService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/reclamations")
@RequiredArgsConstructor
@Tag(name = "Reclamations Management")
public class ReclamationController {
    private final ReclamationService reclamationService;
    private final ReclamationExporter reclamationExporter;

    @GetMapping
    @Operation(summary = "Get all reclamations", description = "Retrieve all reclamations with optional filters for subject, status, and date.")
    @PreAuthorize("hasRole('ROLE_RECEPTIONNAIRE')")
    public ResponseEntity<ApiResponse<Page<ReclamationDto>>> getAll(
            @RequestParam(defaultValue = "0") @Min(value = 0, message = "Page index must be zero or a positive integer") int page,
            @RequestParam(defaultValue = "10") @Min(value = 1, message = "Page size must be at least 1") int size,
            @RequestParam(required = false) String subject,
            @RequestParam(required = false) ReclamationStatus[] statuses,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime date) {
        ReclamationStatus[] effectiveStatuses = (statuses != null && statuses.length > 0) ? statuses : null;
        LocalDateTime effectiveDate = (date != null) ? date : null;

        var reclamations = reclamationService.findAll(
                PageRequest.of(page, size), subject, effectiveStatuses, effectiveDate);
        return ApiResponse.success(reclamations, "reclamations loaded succefully");
    }

    @GetMapping("/{reclamationCode}/export")
    @Operation(summary = "Generate PDF", description = "Generate PDF for a reclamation with the given code.")
    public ResponseEntity<byte[]> generatePdf(@PathVariable String reclamationCode)
            throws IOException, InterruptedException {
        var reclamation = reclamationService.findByCode(reclamationCode);
        byte[] content = reclamationExporter.generatePDF(reclamation);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline")
                .body(content);
    }


    @PostMapping("/{reclamationCode}/claim")
    @Operation(summary = "Claim reclamation", description = "Claim a item with the given code.")
    public ResponseEntity<ApiResponse<ItemDto>> claimItem(@PathVariable String reclamationCode) {
        ReclamationDto reclamation = reclamationService.claim(reclamationCode);
        return ApiResponse.success(reclamation.getItem(), "item claimed successfully");
    }

    @GetMapping("/{reclamationCode}/track")
    @Operation(summary = "Track reclamation", description = "Track a reclamation with the given code.")
    public ResponseEntity<ApiResponse<ReclamationDto>> getReclamation(@PathVariable String reclamationCode) {
        var reclamation = reclamationService.findByCode(reclamationCode);
        return ApiResponse.success(reclamation, "reclamation loaded successfully");
    }

    @GetMapping("/{reclamationId}")
    @Operation(summary = "Get reclamation", description = "Get a reclamation with the given id.")
    public ResponseEntity<ApiResponse<ReclamationDto>> getReclamation(@PathVariable Long reclamationId) {
        ReclamationDto reclamation = reclamationService.findById(reclamationId);
        return ApiResponse.success(reclamation, "reclamation loaded successfully");
    }

    @PostMapping("/{reclamationId}/reject")
    @PreAuthorize("hasRole('ROLE_RECEPTIONNAIRE')")
    @Operation(summary = "Reject reclamation", description = "Reject a reclamation with the given id.")
    public ResponseEntity<ApiResponse<ReclamationDto>> rejectReclamation(@PathVariable Long reclamationId) {
        var reclamation = reclamationService.reject(reclamationId);
        return ApiResponse.success(reclamation, "reclamation rejected successfully");
    }

    @PostMapping("/{reclamationId}/accept")
    @PreAuthorize("hasRole('ROLE_RECEPTIONNAIRE')")
    @Operation(summary = "Accept reclamation", description = "Accept a reclamation with the given id.")

    public ResponseEntity<ApiResponse<ReclamationDto>> acceptReclamation(@PathVariable Long reclamationId) {
        ReclamationDto reclamation = reclamationService.accept(reclamationId);
        return ApiResponse.success(reclamation, "reclamation accepted successfully");
    }

    @PostMapping(consumes = {
            MediaType.MULTIPART_FORM_DATA_VALUE,
            MediaType.APPLICATION_JSON_VALUE
    })
    @Operation(summary = "Create reclamation", description = "Create a new reclamation.")
    public ResponseEntity<ApiResponse<String>> createReclamation(
            @ModelAttribute @Valid CreateReclamation reclamation)
            throws IOException {
        reclamationService.save(reclamation);
        return ApiResponse.success(null, "reclamation created successfully");
    }

    @ExceptionHandler({ IOException.class, InterruptedException.class })
    public ResponseEntity<ApiResponse<String>> handleIOException(Exception e) {
        return ApiResponse.error(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, null);
    }
}
