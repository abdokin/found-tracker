package com.foundtracker.web.controller;

import com.foundtracker.web.dto.CreateItem;
import com.foundtracker.web.dto.EditItem;
import com.foundtracker.web.dto.ItemDto;
import com.foundtracker.web.enums.ItemStatus;
import com.foundtracker.web.responses.ApiResponse;
import com.foundtracker.web.service.ItemService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import org.springframework.web.bind.annotation.PostMapping;


@RestController
@RequestMapping("/api/v1/items")
@RequiredArgsConstructor
@Tag(name = "Items")
public class ItemController {
    private final ItemService itemService;

    @GetMapping
    @Operation(summary = "Get all items", description = "Retrieve all items with optional filters for name, status, and date.")
    public ResponseEntity<ApiResponse<Page<ItemDto>>> getAllItems(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String[] status,
            @RequestParam(required = false) String date,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {

        LocalDateTime dateTime = parseDate(date);
        System.out.println("date " + dateTime);
        ItemStatus[] itemStatus = parseStatus(status);
        Page<ItemDto> items = itemService.findAll(PageRequest.of(page, size), name, itemStatus,
                dateTime);
        return ApiResponse.success(items, "items loaded succefully");
    }


    private ItemStatus[] parseStatus(String[] status) {
        if (status == null || status.length == 0) {
            return null;
        }
        ItemStatus[] statuses = new ItemStatus[status.length];
        for (int i = 0; i < status.length; i++) {
            statuses[i] = ItemStatus.valueOf(status[i]);
        }
        return statuses;
    }


    @PostMapping(consumes = {
            MediaType.MULTIPART_FORM_DATA_VALUE,
            MediaType.APPLICATION_JSON_VALUE
    })
    @PreAuthorize("hasRole('ROLE_RECEPTIONNAIRE')")
    @Operation(summary = "Create item", description = "Create a new item.")
    public ResponseEntity<ApiResponse<ItemDto>> addItem(@ModelAttribute @Valid CreateItem item) throws IOException {
        ItemDto saveItem = itemService.save(item);
        return ApiResponse.success(saveItem, "item created succefully");
    }

    @PatchMapping(value = "/{id}", consumes = {
            MediaType.MULTIPART_FORM_DATA_VALUE,
            MediaType.APPLICATION_JSON_VALUE
    })
    @PreAuthorize("hasRole('ROLE_RECEPTIONNAIRE')")
    @Operation(summary = "Edit item", description = "Edit an existing item.")
    public ResponseEntity<ApiResponse<ItemDto>> editItem(@PathVariable Long id,
            @ModelAttribute @Valid EditItem itemDto) {
        ItemDto item = itemService.edit(id, itemDto);
        return ApiResponse.success(item, "item edited succefully");
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_RECEPTIONNAIRE')")
    @Operation(summary = "Delete item", description = "Delete an existing item.")
    public ResponseEntity<ApiResponse<String>> deleteItem(@PathVariable Long id) {
        itemService.delete(id);
        return ApiResponse.success(null, "item deleted succefully");
    }

    @PostMapping("/{reclamationCode}/claim")
    public ResponseEntity<ApiResponse<ItemDto>> claim(@PathVariable String reclamationCode) {
        return ApiResponse.success(itemService.claim(reclamationCode), "item claimed succefully");
    }
    

    private LocalDateTime parseDate(String dateString) {
        if (dateString == null || dateString.isEmpty()) {
            return null;
        }
        try {
            return LocalDateTime.parse(dateString, DateTimeFormatter.ISO_DATE);
        } catch (DateTimeParseException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

}
