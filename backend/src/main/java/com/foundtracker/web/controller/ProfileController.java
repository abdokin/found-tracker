package com.foundtracker.web.controller;

import com.foundtracker.web.dto.ChangePassword;
import com.foundtracker.web.dto.EditProfile;
import com.foundtracker.web.dto.UserDto;
import com.foundtracker.web.exception.FieldsNotMatch;
import com.foundtracker.web.exception.IncorrectPasswordException;
import com.foundtracker.web.exception.UnauthorizedException;
import com.foundtracker.web.model.User;
import com.foundtracker.web.responses.ApiResponse;
import com.foundtracker.web.service.UserService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/profile")
@RequiredArgsConstructor
@Tag(name = "Profile")
public class ProfileController {
    private final UserService userService;

    @PatchMapping("/change-password")
    @Operation(summary = "Change password", description = "Change password of current user.")
    public ResponseEntity<ApiResponse<String>> changePassword(
            @RequestBody @Valid ChangePassword request,
            @AuthenticationPrincipal User user) throws IncorrectPasswordException, FieldsNotMatch {
        userService.changePassword(request);
        return ApiResponse.success(null, "password changed successfully");
    }

    @PatchMapping("/update-info")
    @Operation(summary = "Update user info", description = "Update user info of current user.")
    public ResponseEntity<ApiResponse<UserDto>> updateInfo(@RequestBody @Valid EditProfile input) {
        return ApiResponse.success(userService.editProfile(input), "profile updated successfully");
    }

    @GetMapping("/current-user")
    @Operation(summary = "Get current user", description = "Get current user.")
    public ResponseEntity<ApiResponse<UserDto>> getCurrentUser(@AuthenticationPrincipal User user) {
        if (user == null) {
            throw new UnauthorizedException("Not authenticated.");
        }
        UserDto userDto = UserDto.mapToUserDto(user);
        return ApiResponse.success(userDto, "user loaded successfully");
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<ApiResponse<String>> handleUnauthorizedException(UnauthorizedException ex) {
        return ApiResponse.error(ex.getMessage(), HttpStatus.UNAUTHORIZED, null);
    }

    @ExceptionHandler({ IncorrectPasswordException.class })
    public ResponseEntity<ApiResponse<String>> handleIncorrectPasswordException(Exception e) {
        return ApiResponse.error(e.getMessage(), HttpStatus.BAD_REQUEST, null);
    }

    @ExceptionHandler({ FieldsNotMatch.class })
    public ResponseEntity<ApiResponse<String>> handleFieldsNotMatch(Exception e) {
        var validationError = new HashMap<String, String>();
        validationError.put("password", "passwords do not match");
        validationError.put("confirmationPassword", "passwords do not match");
        return ApiResponse.error(e.getMessage(), HttpStatus.BAD_REQUEST, validationError);

    }
}
