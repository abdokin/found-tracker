package com.foundtracker.web.controller;

import com.foundtracker.web.dto.LoginDto;
import com.foundtracker.web.dto.RegisterDto;
import com.foundtracker.web.responses.ApiResponse;
import com.foundtracker.web.service.AuthenticationService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@Tag(name = "Authentication")
public class AuthenticationController {

  private final AuthenticationService service;

  @PostMapping("/register")
  @Operation(summary = "Register user", description = "Register a new user.")  
  public ResponseEntity<ApiResponse<Object>> register(@RequestBody @Valid RegisterDto request) {
    service.register(request);
    return ApiResponse.success(null, "user registered successfully");
  }


  @PostMapping("/authenticate")
  @Operation(summary = "Authenticate user", description = "Authenticate an existing user.")
  public ResponseEntity<ApiResponse<String>> authenticate(@RequestBody @Valid LoginDto request) {
    String token = service.authenticate(request);
    return ApiResponse.success(token, "Welcome back");
  }
}
