package com.foundtracker.web.exception;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.foundtracker.web.responses.ApiResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiResponse<String>> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> validationErrors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = null;
            String errorMessage = null;
            if (error instanceof FieldError) {
                fieldName = ((FieldError) error).getField();
                errorMessage = error.getDefaultMessage();
            }
            if (fieldName != null && fieldName.equals("images")) {
                errorMessage = "Invalid image format or no images provided";
            }
            validationErrors.put(fieldName, errorMessage);
        });
        // message must be the first message in the list
        String messgae = ex.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        return ApiResponse.error(messgae, HttpStatus.BAD_REQUEST, validationErrors);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ApiResponse<String>> handleBadCredentialsException(BadCredentialsException e) {
        return ApiResponse.error(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ApiResponse<String>> handleRuntimeException(RuntimeException e) {
        return ApiResponse.error(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ApiResponse<String>> handleDataIntegrityViolationException(
            DataIntegrityViolationException e) {
        String errorMessage = e.getMessage();
        String fieldName = extractFieldName(errorMessage);
        Map<String, String> validationErrors = new HashMap<>();
        validationErrors.put(fieldName, fieldName + " already exists");
        return ApiResponse.error(errorMessage, HttpStatus.BAD_REQUEST, validationErrors);
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<ApiResponse<String>> handleNoSuchElementException(NoSuchElementException e) {
        return ApiResponse.error(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    // org.springframework.security.authentication.DisabledException: User is
    // disabled
    @ExceptionHandler(DisabledException.class)
    public ResponseEntity<ApiResponse<String>> disabledException(DisabledException e) {
        return ApiResponse.error(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    public static String extractFieldName(String errorMessage) {
        Pattern pattern = Pattern.compile("Key \\((.*?)\\)");
        Matcher matcher = pattern.matcher(errorMessage);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }
}
