# Found Tracker PFE Project
PFE project for university FSAC 2024.

## Description


## Getting started
#### First install depenedencies 
1. Bun (Alternative fast to nodejs)
```
    curl -fsSL https://bun.sh/install | bash
```
2. openJDK
3. PostgreSQL
4. Maven

#### Setup backend 
1. change project configuration
2. run project using maven 
```
    mvn spring-boot:run
```