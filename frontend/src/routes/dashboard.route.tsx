import NavBar from '@/components/manager-nav-bar';
import { userQueryOptions } from '@/services/user-service';
import { Outlet, createFileRoute, redirect } from '@tanstack/react-router'

export const Route = createFileRoute('/dashboard')({
  component: () => <>
    <div className="flex min-h-screen flex-1 flex-col bg-muted/40">
      <NavBar />
      <main className='flex flex-grow w-full'>
        <Outlet />
      </main>
    </div>
  </>,
  beforeLoad: async ({ context }) => {
    console.log("loading dashboard");
    const queryClient = context.queryClient;
    try {
      const data = await queryClient.fetchQuery(userQueryOptions);
      if (!data) {
        console.log("data ", data);
      }
      return data;
    } catch (error) {
      throw redirect({ to: '/login' });
    }
  },
})