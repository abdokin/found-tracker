import { createLazyFileRoute } from '@tanstack/react-router'
export const Route = createLazyFileRoute('/')({
  component: () => {
  return (
    <div className="container text-white">
    <section className="w-full py-12 md:py-24 oveflow-scroll">
      <div className="container px-4 md:px-6">
        <div className="grid gap-6 lg:grid-cols-[1fr_400px] lg:gap-12 xl:grid-cols-[1fr_600px]">
          <div className="flex flex-col justify-center space-y-4  w-full">
            <div className="space-y-2 ">
              <h1 className="text-3xl font-bold tracking-tighter sm:text-5xl xl:text-6xl/none text-primary">
                Never Lose Your Belongings Again
              </h1>
              <p className="max-w-[600px] text-gray-500 md:text-xl dark:text-gray-400">
                Our Lost Objects Tracker app helps you locate your lost items in real-time, connect with a community
                of users, and recover your belongings with ease.
              </p>
            </div>
            <div className="flex flex-col gap-2 min-[400px]:flex-row">
              {/* TODO: fix dashboard links */}
              {/* {!authStore.isAuthenticated && <>
                <Link to="/login">
                  <Button>Login</Button>
                </Link>
                <Link to="/register">
                  <Button variant={'secondary'}>Register</Button>
                </Link></>}
              {authStore.isAdmin() && <>
                <Link to="/dashboard/manager">
                  <Button>Admin Dashboard</Button>
                </Link>
              </>}
              {authStore.isAuthenticated && !authStore.isAdmin() && <>
                <Link to="/dashboard/client">
                  <Button>Dashboard</Button>
                </Link>
              </>} */}
            </div>
          </div>
          <img
              alt="Lost Objects Tracker"
              className="mx-auto aspect-video overflow-hidden rounded-xl object-bottom sm:w-full lg:order-last lg:aspect-square"
              height="841"
              src="/logo.png"
          />
        </div>
      </div>
    </section>

    {/* Features Section */}
    <section className="py-12 bg-primary text-white">
      <div className="container mx-auto">
        <h2 className="text-3xl font-bold mb-8">Key Features</h2>
        <ul className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6">
          <li>
            <h3 className="text-xl font-semibold mb-2">Real-time Tracking</h3>
            <p>Track your lost items in real-time using our advanced tracking technology.</p>
          </li>
          <li>
            <h3 className="text-xl font-semibold mb-2">Easy Recovery</h3>
            <p>Recover your belongings with ease through our intuitive recovery process.</p>
          </li>
          <li>
            <h3 className="text-xl font-semibold mb-2">Secure Reclamation</h3>
            <p>Safely reclaim your lost items with our secure reclamation process, ensuring the protection of your
              personal information.</p>
          </li>
        </ul>
      </div>
    </section>

  </div>
  )
}
})