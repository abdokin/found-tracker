import { createRootRouteWithContext, Outlet } from '@tanstack/react-router'
import { TanStackRouterDevtools } from '@tanstack/router-devtools'
import Footer from "@/components/footet.tsx";
import Navbar from "@/components/main-nav-bar.tsx";
import { Toaster } from "@/components/ui/sonner"

import { type QueryClient } from "@tanstack/react-query";

interface MyRouterContext {
    queryClient: QueryClient;
}
export const Route = createRootRouteWithContext<MyRouterContext>()({
    onError: (err) => {
        console.error(err)
    },
    component: () => (
        <main className="flex min-h-screen flex-1 flex-col bg-muted/40">
            <Navbar />
            <div className='flex flex-1 justify-center items-center'>
                <Outlet />
            </div>
            <Footer />
            <Toaster />
            {/* <TanStackRouterDevtools /> */}
        </main>
    ),
})
