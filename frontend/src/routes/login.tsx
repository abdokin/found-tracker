import { createFileRoute, Link, useNavigate } from '@tanstack/react-router'
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { useForm } from "react-hook-form";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { toast } from 'sonner';
import { useMutation } from '@tanstack/react-query';
import { Login as TLogin, userService } from '@/services/user-service';

export const Route = createFileRoute('/login')({
  component: () => <Login />,
})


function Login() {
  const mutation = useMutation({ mutationFn: userService.login })
  const navigate = useNavigate();
  const form = useForm<TLogin>({
    defaultValues: {
      email: "",
      password: "",
    },
  });
  async function onSubmit(values: TLogin) {
    const res = await mutation.mutateAsync(values);
    console.log(res);
    
    if (res.success && res.data) {
      toast.success(res.message);
      localStorage.setItem('token', res.data);
      form.reset();
      navigate({ to: '/dashboard' });
    } else {
      toast.error(res.message);
      if (!res.success && res.errors) {
        Object.entries(res.errors).forEach(([field, message]) => {
          // @ts-ignore
          form.setError(field, {
            message: message,
          });
        });
      }
    }

  }

  return (
    <main className="container flex justify-center items-center ">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <Card className="w-full max-w-sm mx-auto">
            <CardHeader>
              <CardTitle className="text-2xl">Login</CardTitle>
              <CardDescription>
                Enter your login information below to login to your account.
              </CardDescription>
            </CardHeader>
            <CardContent className="grid gap-4">
              <div className="grid gap-2">
                <FormField
                  control={form.control}
                  name="email"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Email</FormLabel>
                      <FormControl>
                        <Input placeholder="example@mail.com" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid gap-2">
                <FormField
                  control={form.control}
                  name="password"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Password</FormLabel>
                      <FormControl>
                        <Input {...field} type="password" placeholder="your password" />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </CardContent>
            <CardFooter className="flex flex-col">
              <Button className="w-full">Sign in</Button>
              <div className="mt-4 text-center text-sm">
                Don&apos;t have an account?{" "}
                <Link to="/register" className="underline">
                  Sign up
                </Link>
              </div>
            </CardFooter>
          </Card>
        </form>
      </Form>
    </main>
  )
}