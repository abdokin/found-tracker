import { createFileRoute, useNavigate } from '@tanstack/react-router'
import React from "react";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { useForm } from "react-hook-form";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { toast } from "sonner";
import { RegisterInput, userService } from '@/services/user-service';
import { useMutation } from '@tanstack/react-query';
import { Link } from '@tanstack/react-router';
import { Textarea } from '@/components/ui/textarea';

export const Route = createFileRoute('/register')({
  component: function () {
    const mutation = useMutation({ mutationFn: userService.register })

    const navigate = useNavigate();
    const form = useForm<RegisterInput>({
      defaultValues: {
        email: "",
        password: "",
        firstname: "",
        lastname: "",
        dateOfBirth: "",
        cne: "",
        phone: "",
        address: "",
      },
    });
    async function onSubmit(values: RegisterInput) {
      try {

        const res = await userService.register(values);
        console.log(res);

        if (res.success && res.data) {
          toast.success(res.message);
          form.reset();
          navigate({ to: '/login' });
        }
      } catch (res: any) {
        toast.error(res.message);
        if (!res.success && res.errors) {
          Object.entries(res.errors).forEach(([field, message]) => {
            // @ts-ignore
            form.setError(field, {
              message: message,
            });
          });
        }
      }
    }
    return (
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className='w-full max-w-3xl'>
          <Card className="w-full">
            <CardHeader>
              <CardTitle className="text-2xl">Create Account</CardTitle>
              <CardDescription>
                Enter your email below to create your account.
              </CardDescription>
            </CardHeader>
            <CardContent className="grid grid-cols-2 gap-4 w-full">
              <div className="grid gap-2">
                <FormField
                  control={form.control}
                  name="firstname"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>First Name</FormLabel>
                      <FormControl>
                        <Input placeholder="first name" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid gap-2">
                <FormField
                  control={form.control}
                  name="lastname"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Last Name</FormLabel>
                      <FormControl>
                        <Input placeholder="Last name" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid gap-2">
                <FormField
                  control={form.control}
                  name="email"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Email</FormLabel>
                      <FormControl>
                        <Input placeholder="email@example.com" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid gap-2">
                <FormField
                  control={form.control}
                  name="phone"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Phone</FormLabel>
                      <FormControl>
                        <Input placeholder="+21200000000" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid gap-2">
                <FormField
                  control={form.control}
                  name="cne"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>CNE</FormLabel>
                      <FormControl>
                        <Input placeholder="CNE" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid gap-2">
                <FormField
                  control={form.control}
                  name="dateOfBirth"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Date Of Birth</FormLabel>
                      <FormControl>
                        <Input placeholder="" {...field} type='date' />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid gap-2">
                <FormField
                  control={form.control}
                  name="password"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Password</FormLabel>
                      <FormControl>
                        <Input {...field} type="password" placeholder='password' />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid gap-2 col-span-2">
                <FormField
                  control={form.control}
                  name="address"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Address</FormLabel>
                      <FormControl>
                        <Textarea placeholder="" {...field} ></Textarea>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>


            </CardContent>
            <CardFooter className="flex flex-col">
              <Button className="w-full" type='submit'>Sign up</Button>
              <div className="mt-4 text-center text-sm">
                Already have an account?{" "}
                <Link to="/login" className="underline">
                  Sign In
                </Link>
              </div>
            </CardFooter>
          </Card>
        </form>
      </Form>
    );
  }
}
)