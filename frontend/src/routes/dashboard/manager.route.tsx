import { userQueryOptions } from '@/services/user-service';
import { createFileRoute, redirect } from '@tanstack/react-router'

export const Route = createFileRoute('/dashboard/manager')({
  beforeLoad: async ({ context }) => {
    const queryClient = context.queryClient;
    const data = await queryClient.fetchQuery(userQueryOptions);
    if (!data) throw redirect({ to: '/login' });
    // if (data.role !== "RECEPTIONNAIRE") throw redirect({ to: "/dashboard/track-reclamation" })
    return data;
  }
})