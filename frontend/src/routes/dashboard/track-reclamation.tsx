import { createFileRoute } from '@tanstack/react-router'
import { Badge } from "@/components/ui/badge";
import { Button } from "@/components/ui/button";
import {
  Carousel,
  CarouselContent,
  CarouselItem,
} from "@/components/ui/carousel";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Textarea } from "@/components/ui/textarea";
import { useState } from 'react';
import { useMutation, useQuery } from '@tanstack/react-query';
import reclamationService from '@/services/reclamation-service';

export const Route = createFileRoute('/dashboard/track-reclamation')({
  component: () => {
    const [trackingCode, setTrackingCode] = useState("");
    const exportMutation = useMutation({
      mutationFn: reclamationService.exportReclamation,
    })
    const { refetch, isLoading, data: reclamation } = useQuery({
      queryKey: ["findReclamation", trackingCode],
      retry: false,
      queryFn: () => reclamationService.findReclamation(trackingCode),
    })

    return (
      <div className="container mx-auto p-4">
        <h1 className="text-3xl font-bold mb-4">Reclamation Tracking</h1>
        <div className="flex items-center mb-4 gap-1 ">
          <Input
            type="text"
            placeholder="Enter tracking code"
            value={trackingCode}
            onChange={(e) => setTrackingCode(e.target.value)}
          />
          <Button
            onClick={async () => refetch()}
          >
            Track
          </Button>
        </div>
        <div>
          <p className="mb-4">
            Welcome to our Reclamation Tracking page. Here, you can easily track
            the status of your reclamation by entering the tracking code provided
            to you.
          </p>

        </div>
        {isLoading && <p>Loading...</p>}
        {!isLoading && !reclamation && <p>No reclamation found</p>}
        {!isLoading && reclamation && (
          <div className="grid grid-cols-1 md:grid-cols-2 gap-8 max-w-6xl mx-auto py-12 px-4">
            <div className="space-y-4">
              <div className="md:flex-shrink-0 px-4 py-2w">
                <Carousel>
                  <CarouselContent>
                    {reclamation.item.images?.map((it, index) => (
                      <CarouselItem key={index}>
                        <img
                          className="h w-full object-cover max-h-56"
                          src={"/api/files/" + it?.imageUrl}
                          alt={reclamation.item.name}
                        />
                      </CarouselItem>
                    ))}
                  </CarouselContent>
                </Carousel>
              </div>
              <div className="space-y-2">
                <h2 className="text-2xl font-bold">
                  {reclamation.item.name}
                  <Badge>{reclamation.item.status}</Badge>
                </h2>
                <p className="text-gray-500 dark:text-gray-400">
                  {reclamation.item.description}
                </p>
              </div>
              <div className="space-y-2 pt-8">
                <h2 className="text-2xl font-bold">User Details</h2>
                <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
                  <div>
                    <Label htmlFor="fullName">Full Name</Label>
                    <Input
                      id="fullName"
                      readOnly
                      value={
                        reclamation.user.firstname +
                        " " +
                        reclamation.user.lastname
                      }
                    />
                  </div>
                  <div>
                    <Label htmlFor="cne">CNE</Label>
                    <Input id="cne" readOnly value={reclamation.user.cne} />
                  </div>
                  <div>
                    <Label htmlFor="email">Email</Label>
                    <Input id="email" readOnly value={reclamation.user.email} />
                  </div>
                  <div>
                    <Label htmlFor="phone">Phone</Label>
                    <Input id="phone" readOnly value={reclamation.user.phone} />
                  </div>
                  <div>
                    <Label htmlFor="address">Address</Label>
                    <Textarea id="address" readOnly value={reclamation.user.address} />
                  </div>
                </div>
              </div>
            </div>
            <div className="space-y-4">
              <div className="space-y-4">
                <div className="space-y-2">
                  <h2 className="text-2xl font-bold">Reclamation details</h2>

                  <div>
                    <Label htmlFor="subject">Reclamation Subject</Label>
                    <Input id="subject" readOnly value={reclamation.sujet} />
                  </div>
                  <div>
                    <Label htmlFor="subject">Reclamation Status </Label>
                    <Badge>{reclamation.status}</Badge>
                  </div>
                  <div className="pt-1">
                    <Label htmlFor="description">Reclamation Description</Label>
                    <Textarea id="description" readOnly rows={3}>
                      {reclamation.description}
                    </Textarea>
                  </div>
                </div>
              </div>

              {reclamation.status !== "PENDING" &&
                reclamation.status !== "REJECTED" && (
                  <Button onClick={() => exportMutation.mutateAsync(reclamation)}>
                    Print
                  </Button>
                )}
            </div>
          </div>
        )}
      </div>)
  }
})