import EditProfileForm from '@/components/edit-profile-form'
import ResetPasswordForm from '@/components/reset-password-formt'
import { createFileRoute } from '@tanstack/react-router'

export const Route = createFileRoute('/dashboard/profile')({
  component: () => {

    return (
      <div className="mx-auto grid w-full max-w-6xl gap-2 py-4">
        <div className="grid grid-cols-2 gap-6">
          <EditProfileForm  />
          <ResetPasswordForm />
        </div>
      </div>
    )
  }
})