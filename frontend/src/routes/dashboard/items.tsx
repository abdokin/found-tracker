import { createFileRoute } from '@tanstack/react-router'
import ItemFilters from "@/components/items-fitler";
import ListItems from "@/components/list-items";
import { queryOptions } from '@tanstack/react-query';
import {z} from 'zod'
import itemService, { ItemsFilter } from '@/services/item-service';
const isoDateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d{3})?Z$/;

const itemsFitlersSchema = z.object({
  name: z.string().optional(),
  date: z.string().optional().refine(val => {
    if (!val) return true; // Allow optional
    return isoDateFormat.test(val);
  }, {
    message: "Invalid date format. Expected format: YYYY-MM-DD",
  }),
  status: z.array(z.string()).optional(),
  page: z.number().default(0),
  pageSize: z.number().default(10),
})
export const itemsQueryOptions = (filters?: ItemsFilter) => queryOptions({
  queryKey: ['items'],
  queryFn: () => itemService.loadItems(filters),
});
export const Route = createFileRoute('/dashboard/items')({
  validateSearch: (search: Record<string, unknown>): ItemsFilter => {
    const parsed = itemsFitlersSchema.safeParse(search);
    if (!parsed.success) {
      console.error('Invalid search params', parsed.error);
      throw new Error('Invalid search params'+parsed.error);
    }
    return {
      ...parsed.data,date: parsed.data.date  ? new Date(parsed.data.date) : undefined};
  },
  loaderDeps: ({ search }:{search: ItemsFilter}) => ({...search}),
  loader: ({ deps,context }) => context.queryClient.fetchQuery(itemsQueryOptions(deps)),
  
  component: () => <main className="flex items-start ">
    <ItemFilters />
    <ListItems  />
  </main>
})