import { createFileRoute } from '@tanstack/react-router'
import ListReclamations from "@/components/list-reclamations";
import ReclamationsFilters from "@/components/reclamations-filter";
import { queryOptions } from '@tanstack/react-query';
import {z} from 'zod'
import reclamationService, { ReclamationFilter } from '@/services/reclamation-service';
const isoDateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d{3})?Z$/;

const itemsFitlersSchema = z.object({
  name: z.string().optional(),
  date: z.string().optional().refine(val => {
    if (!val) return true; // Allow optional
    return isoDateFormat.test(val);
  }, {
    message: "Invalid date format. Expected format: YYYY-MM-DD",
  }),
  status: z.array(z.string()).optional(),
  page: z.number().default(0),
  pageSize: z.number().default(10),
})
export const reclamationsQueryOptions = (filters?: ReclamationFilter) => queryOptions({
  queryKey: ['reclamations'],
  queryFn: () => reclamationService.loadReclations(filters),
});

export const Route = createFileRoute('/dashboard/manager/reclamations')({
  
  validateSearch: (search: Record<string, unknown>): ReclamationFilter => {
    // validate and parse the search params into a typed state
    const parsed = itemsFitlersSchema.safeParse(search);
    if (!parsed.success) {
      throw new Error('Invalid search params'+parsed.error);
    }
    return {
      ...parsed.data,date: parsed.data.date  ? new Date(parsed.data.date) : undefined};
  },
  // beforeLoad: async ({ search,context }) => context.queryClient.ensureQueryData(itemsQueryOptions(search)),
  loaderDeps: ({ search }:{search: ReclamationFilter}) => ({...search}),
  loader: ({ deps,context }) => context.queryClient.fetchQuery(reclamationsQueryOptions(deps)),
  component: () => (
    <div className="py-8 px-4 w-full">
    <div className=" mx-auto">
      <div className="flex items-center gap-4">
        <div className="flex item-center py-4">
          <ReclamationsFilters />
        </div>
      </div>
      <div className="border rounded-md">
        <ListReclamations />
      </div>
    </div>
  </div>
  )
})