import ListUsers from '@/components/list-users';
import UsersFilter from '@/components/users-filter';
import { UserFilter, userService } from '@/services/user-service';
import { queryOptions } from '@tanstack/react-query';
import { createFileRoute } from '@tanstack/react-router'
import { z } from 'zod'
const isoDateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d{3})?Z$/;
export const usersQueryOptions = (filters?: UserFilter) => queryOptions({
  queryKey: ['users'],
  queryFn: () => userService.loadUsers(filters),
})
const userFilterSchema = z.object({
  name: z.string().optional(),
  status: z.array(z.string()).optional(),
  date: z.string().optional().refine(val => {
    if (!val) return true; // Allow optional
    return isoDateFormat.test(val);
  }, {
    message: "Invalid date format. Expected format: YYYY-MM-DD",
  }),
  page: z.number().default(0),
  pageSize: z.number().default(10),
})
export const Route = createFileRoute('/dashboard/manager/users')({
  validateSearch: (search: Record<string, unknown>): UserFilter => {
    // validate and parse the search params into a typed state
    const parsed = userFilterSchema.safeParse(search);
    if (!parsed.success) {
      console.error('Invalid search params', parsed.error);
      throw new Error('Invalid search params' + parsed.error);
    }
    return {
      ...parsed.data, date: parsed.data.date ? new Date(parsed.data.date) : undefined
    };
  },
  loaderDeps: ({ search }: { search: UserFilter }) => ({ ...search }),
  loader: ({ deps, context }) => context.queryClient.fetchQuery(usersQueryOptions(deps)),
  component: () =>
    <div className="py-8 px-4 w-full">
      <div className=" mx-auto">
        <div className="flex items-center gap-4">
          <div className="flex item-center py-4">
            <UsersFilter />
          </div>
        </div>
        <div className="border rounded-md">
          <ListUsers />
        </div>
      </div>
    </div>,

})