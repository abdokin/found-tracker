import { createFileRoute } from '@tanstack/react-router'
import { Card, CardHeader, CardTitle, CardContent } from "@/components/ui/card";
import { Overview } from "@/components/overview";
import { queryOptions, useQuery } from '@tanstack/react-query';
import { userService } from '@/services/user-service';

 
export const overviewQueryOptions =  queryOptions({
  queryKey: ['overview'],
  queryFn: userService.getStates,
});
export const Route = createFileRoute('/dashboard/manager/')({
  loader: ({ context }) => context.queryClient.fetchQuery(overviewQueryOptions),
  component: () =>  {
    const {data,isLoading} = useQuery(overviewQueryOptions);
    if(isLoading) return <div>Loading...</div>
    if(!data) return <div>No data</div>
   return ( <main className="flex-col md:flex w-full">
      <div className="flex-1 space-y-4 p-8 pt-6">
        <div className="flex items-center justify-between space-y-2">
          <h2 className="text-3xl font-bold tracking-tight">Dashboard</h2>
        </div>

        <div className="grid gap-4 md:grid-cols-2 lg:grid-cols-3">
          <Card>
            <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
              <CardTitle className="text-xl font-medium">
                Reclamations
              </CardTitle>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                className="h-4 w-4 text-muted-foreground"
              >
                <path d="M12 2v20M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6" />
              </svg>
            </CardHeader>
            <CardContent>
              <div className="text-2xl font-bold">
                {data?.totalReclamations}
              </div>
            </CardContent>
          </Card>
          <Card>
            <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
              <CardTitle className="text-xl font-medium">
                Total Items
              </CardTitle>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                className="h-4 w-4 text-muted-foreground"
              >
                <rect width="20" height="14" x="2" y="5" rx="2" />
                <path d="M2 10h20" />
              </svg>
            </CardHeader>
            <CardContent>
              <div className="text-2xl font-bold">
                {data.totalItems}
              </div>
            </CardContent>
          </Card>
          <Card>
            <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
              <CardTitle className="text-xl font-medium">Total Users</CardTitle>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                className="h-4 w-4 text-muted-foreground"
              >
                <path d="M22 12h-4l-3 9L9 3l-3 9H2" />
              </svg>
            </CardHeader>
            <CardContent>
              <div className="text-2xl font-bold">
                {data.totalUsers}
              </div>
            </CardContent>
          </Card>
        </div>
        <div className="grid gap-4 md:grid-cols-2 lg:grid-cols-8">
          <Card className="col-span-4">
            <CardHeader>
              <CardTitle>Overview items monthly</CardTitle>
            </CardHeader>
            <CardContent className="pl-2">
              {data.itemMonthlyCount && (
                <Overview data={data.itemMonthlyCount} />
              )}
            </CardContent>
          </Card>

          <Card className="col-span-4">
            <CardHeader>
              <CardTitle>Overview Reclamation monthly</CardTitle>
            </CardHeader>
            <CardContent className="pl-2">
              {data.reclamationMonthlyCount && (
                <Overview data={data.reclamationMonthlyCount} />
              )}
            </CardContent>
          </Card>
          {/* <Card className="col-span-8">
                    <CardHeader>
                        <CardTitle>Recent Users</CardTitle>
                        <CardDescription>
                        </CardDescription>
                    </CardHeader>
                    <CardContent>
                        <RecentUsers />
                    </CardContent>
                </Card> */}
        </div>
      </div>
    </main>)
  }
})