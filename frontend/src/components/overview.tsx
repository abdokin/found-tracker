import { State } from "@/services/user-service";
import { Bar, BarChart, ResponsiveContainer, XAxis, YAxis } from "recharts";

export function Overview({ data }: { data: State[] }) {
  console.log(JSON.stringify(data));

  // Assuming each State object has 'name' and 'count' attributes
  const formattedData = data.map((item) => ({
    name: item.month,
    total: item.count,
  }));

  return (
    <>
    {/* {JSON.stringify(data)} */}
    {data.length < 1 ? 
      <p className="text-center text-3xl">
        No Data
      </p>
    : null}
    <ResponsiveContainer width="100%" height={350}>
   
      <BarChart data={formattedData}>
        <XAxis
          dataKey="name"
          stroke="#888888"
          fontSize={12}
          tickLine={false}
          axisLine={false}
        />
        <YAxis
          stroke="#888888"
          fontSize={12}
          tickLine={false}
          axisLine={false}
        />
        <Bar
          dataKey="total"
          fill="currentColor"
          radius={[4, 4, 0, 0]}
          className="fill-primary"
        />
      </BarChart>
    </ResponsiveContainer>
    </>
  );
}
