import ItemCard from "./item-card";
import Pagination from "./pagination";
import AddItemForm from "./add-item-form";
import ClaimItem from "./claim-item";
import { useQuery, useSuspenseQuery } from "@tanstack/react-query";
import { itemsQueryOptions } from "@/routes/dashboard/items";
import { useNavigate, useSearch } from "@tanstack/react-router";
import { userQueryOptions } from "@/services/user-service";
function ListItems() {
  const { data: user } = useQuery(userQueryOptions);
  const search = useSearch({ from: "/dashboard/items" });
  const { data: { content: items, totalPages, totalElements, pageable } } = useSuspenseQuery(itemsQueryOptions())
  const navigate = useNavigate();

  return (
    <div className="px-2">
      {items.length > 0 && (
        <>
          <div className="flex justify-end py-4 items-center gap-2">
            {user?.role === "RECEPTIONNAIRE" && <AddItemForm />}
            {user?.role === "RECEPTIONNAIRE" && <ClaimItem />}
          </div>
          <div className="grid grid-cols-1 lg:grid-cols-2 xl:grid-cols-3 gap-4 ">
            {items.map((it) => (
              <ItemCard key={it.id} item={it} />
            ))}
          </div>

        </>
      )}
      {items.length == 0 && (
        <div className="p-16">
          <h1 className="text-xl mx-auto text-center">
            No Items Found  {user?.role === "RECEPTIONNAIRE" && <AddItemForm />}
          </h1>
        </div>
      )}
      <div className="flex items-center p-4">
        <Pagination
          goToPage={(value: number) => {
            navigate({ to: "", search: { ...search, page: value, pageSize: pageable.pageSize } })
          }}
          totalPages={totalPages + 1}
          pageSize={pageable.pageNumber}
          pageNumber={pageable.pageSize}
        />
        <div className="text-xs text-muted-foreground">
          Showing <strong>1-{pageable.pageSize}</strong> of{" "}
          <strong>{totalElements}</strong> items
        </div>
      </div>
    </div>
  );
}
export default ListItems;
