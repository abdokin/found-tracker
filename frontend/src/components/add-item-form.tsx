import { useState } from "react";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Dialog, DialogContent, DialogTrigger } from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { useForm } from "react-hook-form";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { toast } from "sonner";
import { PlusCircle } from "lucide-react";
import { Textarea } from "./ui/textarea";
import ImagesUpload from "./image-upload";
import { useMutation } from "@tanstack/react-query";
import { useNavigate, useSearch } from "@tanstack/react-router";
import itemService, { CreateItem } from "@/services/item-service";

export default function AddItemForm() {
  const [open, setOpen] = useState(false);
  const search = useSearch({ from: "/dashboard/items" });
  const navigate = useNavigate();
  const mutate = useMutation({
    mutationFn: itemService.createItem,
    onSuccess: () => {
      navigate({ to: "/dashboard/items", search })
    }
  })
  const form = useForm<CreateItem>({
    defaultValues: {
      name: "",
      description: "",
      images: [],
    },
  });
  const addImages = (images: File[]) => {
    console.log("updating images", images);
    form.setValue("images", images);
  };
  async function onSubmit(values: CreateItem) {
    const res = await mutate.mutateAsync(values);
    if (res.success) {
      form.reset();
      toast.success(res.message);
      setOpen(false);
    }
    if (!res.success && res.errors) {
      Object.entries(res.errors).forEach(([field, message]) => {
        // @ts-ignore
        form.setError(field, {
          message: message,
        });
      });
      toast.error(res.message);

    }
  }
  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>
        <Button size="sm" className="h-8 gap-1">
          <PlusCircle className="h-3.5 w-3.5" />
          <span className="sr-only sm:not-sr-only sm:whitespace-nowrap">
            Add Item
          </span>
        </Button>
      </DialogTrigger>
      <DialogContent className=" p-0">
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="">
            <Card>
              <CardHeader>
                <CardTitle className="text-2xl">Create Item</CardTitle>
                <CardDescription>report item found</CardDescription>
              </CardHeader>
              <CardContent className="grid gap-8">
                <div className="grid gap-2">
                  <FormField
                    control={form.control}
                    name="name"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Name</FormLabel>
                        <FormControl>
                          <Input placeholder="enter item name" {...field} />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </div>
                <div className="grid gap-2">
                  <FormField
                    control={form.control}
                    name="description"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Description</FormLabel>
                        <FormControl>
                          <Textarea
                            placeholder="enter item description"
                            {...field}
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </div>
                <div className="grid gap-2">
                  <FormField
                    control={form.control}
                    name="images"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Images</FormLabel>
                        <FormControl>
                          <ImagesUpload<CreateItem, "images">
                            field={field}
                            addImages={addImages}
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </div>
              </CardContent>
              <CardFooter>
                <Button type="submit" className="w-full">
                  Save
                </Button>
              </CardFooter>
            </Card>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}
