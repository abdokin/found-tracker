import { Menu, Search } from "lucide-react";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Sheet, SheetContent, SheetTrigger } from "@/components/ui/sheet";
import { cx } from "class-variance-authority";
import NotificationsList from "./notifications-list";
import UserNav from "./user-nav";
import { useQuery } from "@tanstack/react-query";
import { Link } from "@tanstack/react-router";
import { userQueryOptions } from "@/services/user-service";


function NavBar() {
  const { isPending, error, data: user } = useQuery(userQueryOptions);

  if (isPending) return "loading";
  if (error || !user) return "not logged in";

  console.log("user", user);

  let links = [
    { path: "/dashboard", label: "Overview", show: user.role.localeCompare("RECEPTIONNAIRE") === 0 },
    { path: "/dashboard/items", label: "Items", show: true },
    { path: "/dashboard/track-reclamation", label: "Reclamations Track", show: true },
    { path: "/dashboard/manager/users", label: "Users",show: user.role.localeCompare("RECEPTIONNAIRE") === 0 },
    { path: "/dashboard/manager/reclamations", label: "Reclamations",show: user.role.localeCompare("RECEPTIONNAIRE") === 0 },
  ];



  return (
    <header className="sticky top-0 flex h-16 items-center gap-4 border-b bg-primary  px-4 md:px-6  z-10">
      <nav className="hidden flex-col gap-6 text-lg font-medium md:flex md:flex-row md:items-center md:gap-5 md:text-sm lg:gap-6 w-full">
        {links.filter((link) => link.show).map((link) => {
          return (
            <Link
              key={link.path}
              to={link.path}
              className={cx(
                location.pathname === link.path
                  ? "bg-white px-4 py-2 rounded-md text-primary"
                  : " text-white",
                "text-bold text-md transition-all"
              )}
            >
              {link.label}
            </Link>
          );
        })}
      </nav>
      <Sheet>
        <SheetTrigger asChild>
          <Button variant="outline" size="icon" className="shrink-0 md:hidden">
            <Menu className="h-5 w-5" />
            <span className="sr-only">Toggle navigation menu</span>
          </Button>
        </SheetTrigger>
        <SheetContent side="left" className="flex gap-4 flex-col text-lg font-medium">
          {links.filter((link) => link.show).map((link) => {
            return (
              <Link
                key={link.path}
                to={link.path}
                className={cx(
                  location.pathname === link.path
                    ? "bg-white px-4 py-2 rounded-md text-primary font-bold"
                    : " text-black px-4 py-2 rounded-md",
                  "text-bold text-md transition-all"
                )}
              >
                {link.label}
              </Link>
            );
          })}
        </SheetContent>
      </Sheet>
      <div className="flex w-full items-center gap-4 md:ml-auto md:gap-2 lg:gap-4">
        <form className="ml-auto flex-1 sm:flex-initial">
          <div className="relative">
            <Search className="absolute left-2.5 top-2.5 h-4 w-4 text-muted-foreground" />
            <Input
              type="search"
              placeholder="Search objets..."
              className="pl-8 sm:w-[300px] md:w-[200px] lg:w-[300px]"
            />
          </div>
        </form>
        <NotificationsList />
        <UserNav />
      </div>
    </header>
  );
}
export default NavBar;
