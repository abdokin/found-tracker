import { Input } from "./ui/input";
import { Button } from "./ui/button";
import { FacetedFilter } from "./faceted-filter";
import { Cross2Icon } from "@radix-ui/react-icons";
import { useRef, useState } from "react";
import { useNavigate, useSearch } from "@tanstack/react-router";
import {  ReclamationFilter } from "@/lib/types";
function ReclamationsFilters() {
  const search = useSearch({from: "/dashboard/manager/reclamations"});
  const [filters, setFilters] = useState<ReclamationFilter>(search);
  const navigate = useNavigate();
  const ref = useRef<HTMLInputElement>(null);
  
  const clearFilters = () => {
    const defaultFilters: ReclamationFilter = {
        sujet: undefined,
        date: undefined,
        status: undefined,
        page: search.page,
        pageSize: search.pageSize,
    }
    setFilters(defaultFilters);
    // @ts-ignore
    ref.current.value = "";
    navigate({to: "/dashboard/manager/reclamations",search:defaultFilters,resetScroll:true});
  };
  const isFiltred = (filters.sujet || filters.date || filters.status);
  return (
    <div className="flex items-center justify-between">
      <div className="flex  items-center gap-2">
        <Input
          placeholder="Search Reclamations..."
          ref={ref}
          name="sujet"
          onChange={(event) => setFilters({ ...filters, sujet: event.target.value })}
          className="w-[150px] lg:w-[250px]"
        />
        <FacetedFilter
          title="Status"
          selectedValues={new Set(filters.status)}
          setSelectedValues={(statuses) => {
            const statusesArray = Array.from(statuses);
            setFilters({ ...filters, status: statusesArray });
          }}
          setFilterValue={(values?: string[] ) => setFilters({ ...filters, status: values })}
          options={[
            { label: "PENDING", value: "PENDING" },
            { label: "ACCEPTED", value: "ACCEPTED" },
            { label: "REJECTED", value: "REJECTED" },
          ]}
        />
          {isFiltred && (
            <>
            <Button
            variant="ghost"
            onClick={clearFilters}
            className=" px-2 lg:px-3"
          >
            Reset
            <Cross2Icon className="ml-2  w-4" />
          </Button>
          <Button
            onClick={() => navigate({ to: "/dashboard/manager/reclamations" ,search:{...filters,sujet:ref.current?.value ?? "" }})}
            className="px-2 lg:px-3">
            Filter
          </Button>
          </>
          )}
      </div>
    </div>
  );
}

export default ReclamationsFilters;
