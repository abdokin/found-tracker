import { Button } from "@/components/ui/button";
import { useForm } from "react-hook-form";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { toast } from "sonner";
import { Textarea } from "@/components/ui/textarea";
import { Input } from "@/components/ui/input";
import {
  Sheet,
  SheetClose,
  SheetContent,
  SheetDescription,
  SheetFooter,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet";
import { useState } from "react";
import ImagesUpload from "./image-upload";
import reclamationService, { CreateReclamation } from "@/services/reclamation-service";

export default function AddReclamation({ objetId }: { objetId: number }) {
  const [open, setOpen] = useState(false);
  const form = useForm<CreateReclamation>({
    defaultValues: {
      itemId: objetId,
      description: "",
      sujet: "",
      images: [],
    },
  });

  const addImages = (images: File[]) => {
    console.log("updating images", images);
    form.setValue("images", images);
  };
  async function onSubmit(values: CreateReclamation) {
    const res = await reclamationService.createReclamation(values);
    if (res.success) {
      form.reset();
      toast.success(res.message);
      setOpen(false);
    }
    if (!res.success && res.errors) {
      Object.entries(res.errors).forEach(([field, message]) => {
        // @ts-ignore
        form.setError(field, {
          message: message,
        });
      });
      toast.error(res.message);
    }
  }

  return (
    <Sheet onOpenChange={setOpen} open={open}>
      <SheetTrigger>
        <Button size={"sm"}>Add reclamation</Button>
      </SheetTrigger>
      <SheetContent className="sm:max-w-3xl w-full overflow-y-auto max-h-screen">
        <SheetHeader>
          <SheetTitle>Create Reclamation</SheetTitle>
          <SheetDescription>
            If you have lost an object and believe it can be recovered, you can
            create a reclamation to start the process of retrieving your lost
            item. Please provide accurate details about the lost object to help
            expedite the search and recovery process.
          </SheetDescription>
        </SheetHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="w-full  ">
            <div className="flex flex-col gap-8">
              <div className="grid gap-2">
                <FormField
                  control={form.control}
                  name="sujet"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Sujet</FormLabel>
                      <FormControl>
                        <Input
                          placeholder="enter reclamation sujet"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid gap-2">
                <FormField
                  control={form.control}
                  name="description"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Description</FormLabel>
                      <FormControl>
                        <Textarea
                          placeholder="enter reclamation description"
                          rows={5}
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>

              <div className="col-span-2">
                <FormField
                  control={form.control}
                  name="images"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Attachements</FormLabel>
                      <FormDescription>
                        When submitting a reclamation, you can increase its
                        effectiveness by adding attachments. Attachments allow
                        you to provide additional context, evidence, or
                        supporting documents related to your reclamation.{" "}
                      </FormDescription>
                      <FormControl>
                        <ImagesUpload<CreateReclamation, "images">
                          field={field}
                          addImages={addImages}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <SheetFooter className="py-4 mx-auto">
              <SheetClose asChild>
                <Button variant={"secondary"}>Close</Button>
              </SheetClose>
              <Button type="submit">Save changes</Button>
            </SheetFooter>
          </form>
        </Form>
      </SheetContent>
    </Sheet>
  );
}
