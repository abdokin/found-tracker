import { Button } from "./ui/button";
import {
    AlertDialog,
    AlertDialogTrigger,
    AlertDialogContent,
    AlertDialogHeader,
    AlertDialogTitle,
    AlertDialogDescription,
    AlertDialogFooter,
    AlertDialogCancel,
    AlertDialogAction,
} from "./ui/alert-dialog";
import { useMutation, useQuery } from "@tanstack/react-query";
import { toast } from "sonner";
import { useNavigate, useSearch } from "@tanstack/react-router";
import { User, userQueryOptions, userService } from "@/services/user-service";

export default function UserActions(props: {
    user: User;
}) {
    const { data: current } = useQuery(userQueryOptions)
    const search = useSearch({ from: "/dashboard/manager/users" })
    const navigate = useNavigate();
    const blockUserMutation = useMutation({
        mutationFn: userService.blockUser,

        onError: (data) => {
            toast.error(data.message);
        },
        onSuccess: (data) => {
            toast.success(data.message)
            navigate({ to: "/dashboard/manager/users", search })
        }
    })
    const unblockUserMutation = useMutation({
        mutationFn: userService.unblockUser,
        onError: (data) => {
            toast.error(data.message);
        },
        onSuccess: (data) => {
            toast.success(data.message)
            navigate({ to: "/dashboard/manager/users", search })
        }
    })
    return current?.role === "RECEPTIONNAIRE" && (
        <div className="flex gap-2 ">
            {props.user.enabled && (
                <AlertDialog>
                    <AlertDialogTrigger asChild>
                        <Button variant={"destructive"} size={"sm"}>
                            Block
                        </Button>
                    </AlertDialogTrigger>
                    <AlertDialogContent>
                        <AlertDialogHeader>
                            <AlertDialogTitle>
                                Are you absolutely sure?
                            </AlertDialogTitle>
                            <AlertDialogDescription>
                                This action will block this user from all access until you undo this action
                            </AlertDialogDescription>
                        </AlertDialogHeader>
                        <AlertDialogFooter>
                            <AlertDialogCancel>Cancel</AlertDialogCancel>
                            <AlertDialogAction
                                onClick={() => blockUserMutation.mutateAsync(props.user.id)}
                            >
                                Continue
                            </AlertDialogAction>
                        </AlertDialogFooter>
                    </AlertDialogContent>
                </AlertDialog>
            )}
            {!props.user.enabled && (
                <AlertDialog>
                    <AlertDialogTrigger asChild>
                        <Button size={"sm"}>Unblock</Button>
                    </AlertDialogTrigger>
                    <AlertDialogContent>
                        <AlertDialogHeader>
                            <AlertDialogTitle>
                                Are you absolutely sure?
                            </AlertDialogTitle>
                            <AlertDialogDescription>
                                This action will give access to login back to this user.
                            </AlertDialogDescription>
                        </AlertDialogHeader>
                        <AlertDialogFooter>
                            <AlertDialogCancel>Cancel</AlertDialogCancel>
                            <AlertDialogAction
                                onClick={() => unblockUserMutation.mutateAsync(props.user.id)}>
                                Continue
                            </AlertDialogAction>
                        </AlertDialogFooter>
                    </AlertDialogContent>
                </AlertDialog>
            )}
        </div>
    );
}
