import {
  Pagination,
  PaginationContent,
  PaginationItem,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
} from "@/components/ui/pagination";
import { observer } from "mobx-react";

function CustomePagination(props: {
  pageSize: number;
  totalPages: number;
  pageNumber: number;
  goToPage: (value: number) => void;
}) {
  const previousPage = Math.max(props.pageNumber - 1, 0);
  const nextPage = Math.min(props.pageNumber + 1, props.totalPages - 1);

  return (
    <Pagination>
      <PaginationContent>
        <PaginationItem>
          <PaginationPrevious
            aria-disabled={props.pageNumber === 0}
            onClick={() => props.goToPage(previousPage)}
          />
        </PaginationItem>
        {Array.from({ length: props.totalPages }, (_, index) => (
          <PaginationItem key={index}>
            <PaginationLink onClick={() => props.goToPage(index)}>
              {index + 1}
            </PaginationLink>
          </PaginationItem>
        ))}
        <PaginationItem>
          <PaginationNext
            aria-disabled={props.pageNumber === props.totalPages - 1}
            onClick={() => props.goToPage(nextPage)}
          />
        </PaginationItem>
      </PaginationContent>
    </Pagination>
  );
}

export default observer(CustomePagination);
