import { Link } from '@tanstack/react-router'
import { useQuery } from "@tanstack/react-query";
import { Button } from '@/components/ui/button'
import { userQueryOptions } from '@/services/user-service';
const Navbar = () => {
    const { isPending, error, data: user } = useQuery(userQueryOptions);
    if (isPending) {
        return <div>Loading...</div>;
    }
    return (error || !user) && (
        <nav className="text-black p-4  shadow-sm w-full bg-primary relative">
            <div className="px-8 mx-auto flex justify-end items-center text-white">
                <ul className="flex justify-end space-x-4">
                    <li>
                        <Link to="/login">
                            <Button variant={'secondary'} className="[&.active]:font-bold">Login</Button>
                        </Link>
                    </li>
                    <li>
                        <Link to="/register">
                            <Button variant={'secondary'} className="[&.active]:font-bold">Register</Button>
                        </Link>
                    </li>
                </ul>
            </div>
            <div className={'flex items-center gap-1 p-2 border bg-white absolute top-1'}>
                <Link to={'/'} className="[&.active]:font-bold">
                    <img src={'/logo-1.png'} alt="FSAC log" width={200} />
                </Link>
            </div>
        </nav>
    );
};

export default Navbar;
