import { Button } from "@/components/ui/button";
import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { useForm } from "react-hook-form";
import {
    Form,
    FormControl,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form";
import { toast } from "sonner";
import { useMutation } from "@tanstack/react-query";
import { ResetPassword, userService } from "@/services/user-service";

export default function ResetPasswordForm() {
    const resetPasswordMutation = useMutation({
        mutationKey: ["resetPassword"],
        mutationFn: userService.resetPassword,
    })
    const form = useForm<ResetPassword>({
        defaultValues: {
            newPassword: "",
            currentPassword: "",
            confirmationPassword: ""
        },
    });
    async function onSubmit(values: ResetPassword) {
        console.log(values);
        try {
            const res = await resetPasswordMutation.mutateAsync(values);
            if (res.success) {
                toast.success(res.message);
                form.reset();
            }
        } catch (e: any) {
            console.log(e);
            toast.error(e.message);
            if (!e.success && e.errors) {
                Object.entries(e.errors).forEach(([field, message]) => {
                    // @ts-ignore
                    form.setError(field, {
                        message: message,
                    });
                });
            }
        }
    }
    return (
        <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="">
                <Card>
                    <CardHeader>
                        <CardTitle className="text-2xl">Change Password</CardTitle>
                        <CardDescription>
                            eet your Password
                        </CardDescription>
                    </CardHeader>
                    <CardContent className="grid gap-8">
                        <div className="grid gap-2">
                            <FormField
                                control={form.control}
                                name="currentPassword"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>Current Password</FormLabel>
                                        <FormControl>
                                            <Input placeholder="enter your old password" {...field} type="password" />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </div>
                        <div className="grid gap-2">
                            <FormField
                                control={form.control}
                                name="newPassword"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>New Password</FormLabel>
                                        <FormControl>
                                            <Input placeholder="enter your new password" {...field} type="password" />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </div>
                        <div className="grid gap-2">
                            <FormField
                                control={form.control}
                                name="confirmationPassword"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>Confirm New Password</FormLabel>
                                        <FormControl>
                                            <Input placeholder="re-enter your new password" {...field} type="password" />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </div>
                    </CardContent>
                    <CardFooter>
                        <Button className="w-full" disabled={resetPasswordMutation.isLoading}>Save</Button>
                    </CardFooter>
                </Card>
            </form>
        </Form>
    );
}
