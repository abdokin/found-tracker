import { Dialog, DialogContent } from "./ui/dialog";
import { Carousel, CarouselContent, CarouselItem } from "./ui/carousel";
import { Badge } from "./ui/badge";
import { useQuery } from "@tanstack/react-query";
import { userQueryOptions } from "@/services/user-service";
import { Item } from "@/services/item-service";

export default function ItemDetails(props: {
    item: Item;
    open: boolean;
    onOpenChange: (open: boolean) => void;
}) {
    const { data: user } = useQuery(userQueryOptions);

    if (!user) return null;
    return (
        <Dialog open={props.open} onOpenChange={props.onOpenChange}>
            <DialogContent className="">
                <div className="md:flex-shrink-0 px-4 py-2w">
                    <Carousel>
                        <CarouselContent>
                            {props.item.images.map((it, index) => (
                                <CarouselItem key={index}>
                                    <img
                                        className="h w-full object-cover max-h-56"
                                        src={"/api/files/" + it?.imageUrl}
                                        alt={props.item.name}
                                    />
                                </CarouselItem>
                            ))}
                        </CarouselContent>
                    </Carousel>
                </div>
                <div className="space-y-2">
                    <h2 className="text-2xl font-bold flex items-center">
                        {props.item.name}
                        <Badge>{props.item.status}</Badge>
                    </h2>
                    <p className="text-gray-500 dark:text-gray-400">
                        {props.item.description}
                    </p>
                </div>
            </DialogContent>
        </Dialog>
    );
}
