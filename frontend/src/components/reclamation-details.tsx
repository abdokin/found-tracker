import { useState } from "react";
import { Dialog, DialogContent, DialogTrigger } from "./ui/dialog";
import { Button } from "./ui/button";
import { Carousel, CarouselContent, CarouselItem } from "./ui/carousel";
import { Input } from "./ui/input";
import { Label } from "./ui/label";
import { Textarea } from "./ui/textarea";
import {
  AlertDialog,
  AlertDialogTrigger,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogCancel,
  AlertDialogAction,
} from "./ui/alert-dialog";
import { Badge } from "./ui/badge";
import { DownloadIcon, FileIcon } from "lucide-react";
import { useQuery, useMutation } from "@tanstack/react-query";
import { useNavigate, useSearch } from "@tanstack/react-router";
import { userQueryOptions } from "@/services/user-service";
import reclamationService, { Reclamation } from "@/services/reclamation-service";
import { toast } from "sonner";

export default function ReclamationsDetails(props: {
  reclamation: Reclamation;
}) {
  const [dialogOpen, setDialogOpen] = useState(false);
  const { data: user } = useQuery(userQueryOptions);
  const search = useSearch({ from: "/dashboard/manager/reclamations" });
  const navigate = useNavigate();
  const acceptMutation = useMutation({
    mutationFn: reclamationService.acceptReclamation,
    onSuccess: (data) => {
      toast.success(data.message);
      navigate({ to: "/dashboard/manager/reclamations", search: search, resetScroll: true });
      setDialogOpen(false);
    },
    onError: (data) => {
      toast.error(data.message);
    }
  })
  const rejectMutation = useMutation({
    mutationFn: reclamationService.rejectReclamation,
    onSuccess: (data) => {
      toast.success(data.message);
      navigate({ to: "/dashboard/manager/reclamations", search: search, resetScroll: true });
      setDialogOpen(false);

    },
    onError: (data) => {
      toast.error(data.message);
    }
  })
  const exportMutation = useMutation({
    mutationFn: reclamationService.exportReclamation,
    onSuccess: () => {
      setDialogOpen(false);
      navigate({ to: "/dashboard/manager/reclamations", search: search, resetScroll: true });
    }
  })
  if (!user) return null;
  return (
    <Dialog open={dialogOpen} onOpenChange={setDialogOpen}>
      <DialogTrigger>
        <Button variant={"outline"}>Details</Button>
      </DialogTrigger>
      <DialogContent className="max-w-7xl">
        <div className="grid grid-cols-1 md:grid-cols-2 gap-8 max-w-6xl mx-auto py-12 px-4">
          <div className="space-y-4">
            <div className="md:flex-shrink-0 px-4 py-2w">
              <Carousel>
                <CarouselContent>
                  {props.reclamation.item.images.map((it, index) => (
                    <CarouselItem key={index}>
                      <img
                        className="h w-full object-cover max-h-56"
                        src={"/api/files/" + it?.imageUrl}
                        alt={props.reclamation.item.name}
                      />
                    </CarouselItem>
                  ))}
                </CarouselContent>
              </Carousel>
            </div>
            <div className="space-y-2">
              <h2 className="text-2xl font-bold">
                {props.reclamation.item.name}
                <Badge>{props.reclamation.item.status}</Badge>
              </h2>
              <p className="text-gray-500 dark:text-gray-400">
                {props.reclamation.item.description}
              </p>
            </div>
            <div className="space-y-2 pt-8">
              <h2 className="text-2xl font-bold">User Details</h2>
              <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
                <div>
                  <Label htmlFor="fullName">Full Name</Label>
                  <Input
                    id="fullName"
                    readOnly
                    value={
                      props.reclamation.user.firstname +
                      " " +
                      props.reclamation.user.lastname
                    }
                  />
                </div>
                <div>
                  <Label htmlFor="email">Email</Label>
                  <Input
                    id="email"
                    readOnly
                    value={props.reclamation.user.email}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="space-y-4">
            <div className="space-y-4">
              <div className="space-y-2">
                <h2 className="text-2xl font-bold">Reclamation details</h2>

                <div>
                  <Label htmlFor="subject">Recmalamtion Subject</Label>
                  <Input
                    id="subject"
                    readOnly
                    value={props.reclamation.sujet}
                  />
                </div>
                <div>
                  <Label htmlFor="subject">Recmalamtion Status </Label>
                  <Badge>{props.reclamation.status}</Badge>
                </div>
                <div className="pt-1">
                  <Label htmlFor="description">Reclamation Description</Label>
                  <Textarea id="description" readOnly rows={3}>
                    {props.reclamation.description}
                  </Textarea>
                </div>
              </div>
            </div>

            {user?.role === "RECEPTIONNAIRE" && (
              <>
                <div className="space-y-2">
                  <h2 className="text-2xl font-bold">Attachments</h2>
                  <div className="space-y-2">
                    {props.reclamation.images.map((it, index) => (
                      <div
                        key={index}
                        className="flex items-center justify-between border rounded-lg p-4"
                      >
                        <div className="flex items-center gap-4">
                          <FileIcon className="w-6 h-6 text-gray-500" />
                          <div>
                            <p className="font-medium">{it.imageName}</p>
                          </div>
                        </div>
                        <a
                          target="_blank"
                          href={"/api/files/" + it.imageUrl}
                        >
                          <Button size="icon" variant="ghost">
                            <DownloadIcon className="w-5 h-5" />
                            <span className="sr-only">Download</span>
                          </Button>
                        </a>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="flex gap-2 ">
                  {props.reclamation.status === "PENDING" && (
                    <AlertDialog>
                      <AlertDialogTrigger asChild>
                        <Button variant={"destructive"} size={"sm"}>
                          Reject
                        </Button>
                      </AlertDialogTrigger>
                      <AlertDialogContent>
                        <AlertDialogHeader>
                          <AlertDialogTitle>
                            Are you absolutely sure?
                          </AlertDialogTitle>
                          <AlertDialogDescription>
                            This action cannot be undone. This will permanently
                            delete your account and remove your data from our
                            servers.
                          </AlertDialogDescription>
                        </AlertDialogHeader>
                        <AlertDialogFooter>
                          <AlertDialogCancel>Cancel</AlertDialogCancel>
                          <AlertDialogAction
                            onClick={async () => rejectMutation.mutateAsync(props.reclamation.id)}
                          >
                            Continue
                          </AlertDialogAction>
                        </AlertDialogFooter>
                      </AlertDialogContent>
                    </AlertDialog>
                  )}
                  {props.reclamation.status === "PENDING" && (
                    <AlertDialog>
                      <AlertDialogTrigger asChild>
                        <Button size={"sm"}>Accept</Button>
                      </AlertDialogTrigger>
                      <AlertDialogContent>
                        <AlertDialogHeader>
                          <AlertDialogTitle>
                            Are you absolutely sure?
                          </AlertDialogTitle>
                          <AlertDialogDescription>
                            This action cannot be undone. This will permanently
                            delete your account and remove your data from our
                            servers.
                          </AlertDialogDescription>
                        </AlertDialogHeader>
                        <AlertDialogFooter>
                          <AlertDialogCancel>Cancel</AlertDialogCancel>
                          <AlertDialogAction
                            onClick={async () => acceptMutation.mutateAsync(props.reclamation.id)}
                          >
                            Continue
                          </AlertDialogAction>
                        </AlertDialogFooter>
                      </AlertDialogContent>
                    </AlertDialog>
                  )}

                  {props.reclamation.status !== "PENDING" &&
                    props.reclamation.status !== "REJECTED" && (
                      <Button
                        onClick={async () => exportMutation.mutateAsync(props.reclamation)}
                      >
                        Print
                      </Button>
                    )}
                </div>
              </>
            )}
          </div>
          <div className="col-span-2"></div>
        </div>
      </DialogContent>
    </Dialog>
  );
}
