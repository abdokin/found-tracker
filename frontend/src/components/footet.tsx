export default function Footer() {
    return (
        <footer className="bg-black p-6 md:py-12 w-full dark:bg-gray-800 mt-8">
            <div className="container max-w-7xl flex items-center justify-between">
                <div className="flex items-center gap-2">
                    <span className="text-lg font-medium text-white">Lost Objects Tracker</span>
                </div>
                <p className="text-sm text-white ">© 2024 University. All rights reserved.</p>
            </div>
        </footer>
    );
}
