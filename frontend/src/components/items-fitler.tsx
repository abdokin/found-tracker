import { Checkbox } from "./ui/checkbox";
import { Search } from "lucide-react";
import { Input } from "./ui/input";
import { Button } from "./ui/button";
import { Sheet, SheetContent, SheetTrigger } from "@/components/ui/sheet";
import { DatePicker } from "./date-picker";
import { useQuery } from "@tanstack/react-query";
import { useNavigate, useSearch } from "@tanstack/react-router"
import { useState } from "react";
import { userQueryOptions } from "@/services/user-service";
import { ItemsFilter } from "@/services/item-service";

export default function ItemFilters() {
  const search = useSearch({ from: "/dashboard/items" });
  const navigate = useNavigate();
  const [filters, setFilters] = useState<ItemsFilter>(search);
  const { data: user } = useQuery(userQueryOptions);

  const handleStatusCheckbox = (status: string) => {
    // check if status is already selected from filters
    const newStatus = filters.status?.includes(status)
      ? filters.status?.filter((it) => it !== status)
      : [...(filters.status || []), status];
    // update filters
    setFilters({
      ...filters,
      status: newStatus,
    });
  };
  const setName = (name: string) => {
    setFilters({
      ...filters,
      name,
    });
  };

  const setDate = (date: Date | undefined) => {
    setFilters({
      ...filters,
      date,
    });
  };

  const clearFilters = () => {
    setFilters({
      name: undefined,
      status: undefined,
      date: undefined,
      page: search.page,
      pageSize: search.pageSize
    });
    navigate({ to: "/dashboard/items", search: filters });
  };

  const handleFilterSubmit = () => {
    navigate({ to: "/dashboard/items", search: filters })
  }
  return (
    <>

      <div className="hidden md:flex w-[400px] bg-zinc-50 min-h-screen p-4  flex-col gap-4">
        <h1 className="text-xl">Filters</h1>
        <div className="flex flex-col gap-4">
          <div className="relative">
            <Search className="absolute left-2.5 top-2.5 h-4 w-4 text-muted-foreground" />
            <Input
              type="search"
              placeholder="Search objects..."
              className="pl-8 w-full"
              value={filters.name || ""}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
        </div>
        {user?.role == "RECEPTIONNAIRE" &&
          <>
            <h3 className="text-md">By Status</h3>
            <hr />
            <div className="flex flex-col gap-4 px-4 ">
              <div className="flex items-center space-x-2">
                <Checkbox
                  id="claimed"
                  onClick={() => handleStatusCheckbox("CLAIMED")}
                  checked={filters.status?.includes("CLAIMED")}
                />
                <label
                  htmlFor="claimed"
                  className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                >
                  Claimed
                </label>
              </div>
              <div className="flex items-center space-x-2">
                <Checkbox
                  id="found"
                  onClick={() => handleStatusCheckbox("FOUND")}
                  checked={filters.status?.includes("FOUND")}
                />
                <label
                  htmlFor="found"
                  className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                >
                  Found
                </label>
              </div>
            </div>
            <h3 className="pt-4">By Date</h3>
          </>
        }

        <DatePicker
          date={filters.date}
          setDate={setDate}
        />

        <Button onClick={handleFilterSubmit}>Filter</Button>
        <Button onClick={clearFilters} variant={"outline"}>Reset</Button>
      </div>
      <Sheet>
        <SheetTrigger asChild>
          <Button variant="outline" className="shrink-0 md:hidden ">
            Filters
          </Button>
        </SheetTrigger>
        <SheetContent
          side="left"
          className="flex w-[400px] bg-zinc-50 min-h-screen p-4  flex-col gap-4"
        >
          <h1 className="text-xl">Filters</h1>
          <div className="flex flex-col gap-4">
            <div className="relative">
              <Search className="absolute left-2.5 top-2.5 h-4 w-4 text-muted-foreground" />
              <Input
                type="search"
                placeholder="Search objects..."
                className="pl-8 sm:w-[300px] md:w-[200px] lg:w-[300px]"
                value={filters.name || ""}
                onChange={(e) => (filters.name = e.target.value)}
              />
            </div>
          </div>

          {user?.role == "" && (
            <>
              <h3 className="text-md">By Status</h3>
              <hr />
              <div className="flex flex-col gap-4 px-4 ">
                <div className="flex items-center space-x-2">
                  <Checkbox
                    id="claimed"
                    onClick={() => handleStatusCheckbox("CLAIMED")}
                    checked={filters.status?.includes("CLAIMED")}
                  />
                  <label
                    htmlFor="claimed"
                    className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                  >
                    Claimed
                  </label>
                </div>
                <div className="flex items-center space-x-2">
                  <Checkbox
                    id="found"
                    onClick={() => handleStatusCheckbox("FOUND")}
                    checked={filters.status?.includes("FOUND")}
                  />
                  <label
                    htmlFor="found"
                    className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                  >
                    Found
                  </label>
                </div>
              </div>
            </>
          )}

          <h3 className="pt-4">Date Added</h3>
          <hr />

          <DatePicker
            date={filters.date}
            setDate={setDate}
          />
          <Button onClick={handleFilterSubmit} >Filter</Button>
          <Button onClick={clearFilters} variant={'outline'}>Reset</Button>

        </SheetContent>
      </Sheet>
    </>
  );
}

