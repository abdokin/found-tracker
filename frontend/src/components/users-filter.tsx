import { Input } from "./ui/input";
import { Button } from "./ui/button";
import { FacetedFilter } from "./faceted-filter";
import { Cross2Icon } from "@radix-ui/react-icons";
import { useRef, useState } from "react";
import { useNavigate, useSearch } from "@tanstack/react-router";
import { UserFilter } from "@/services/user-service";
function UsersFilter() {
  const search = useSearch({ from: "/dashboard/manager/users" });
  const navigate = useNavigate();
  const [filters, setFilters] = useState<UserFilter>(search);
  const ref = useRef<HTMLInputElement>(null);
  const clearFilters = () => {
    const defaultFilters: UserFilter = {
      name: undefined,
      date: undefined,
      status: undefined,
      page: search.page,
      pageSize: search.pageSize,
    }
    setFilters(defaultFilters);
    // @ts-ignore
    ref.current.value = "";
    navigate({ to: "/dashboard/manager/users", search: defaultFilters, resetScroll: true });
  };
  const isFiltred = (ref.current?.value || filters.name || filters.date || filters.status);

  return (
    <div className="flex items-center justify-between">
      <div className="flex  items-center gap-2">
        <Input
          ref={ref}
          onChange={(event) => setFilters({ ...filters, name: event.target.value })}
          placeholder="Search Users by name..."
          className="w-[150px] lg:w-[250px]"
        />
        <FacetedFilter
          title="Status"
          selectedValues={new Set(filters.status)}
          setSelectedValues={(statuses) => {
            const statusesArray = Array.from(statuses);
            setFilters({ ...filters, status: statusesArray });
          }}
          setFilterValue={(values?: string[]) => setFilters({ ...filters, status: values })}
          options={[
            { label: "ACTIVE", value: "true" },
            { label: "DESABLED", value: "false" },
          ]}
        />
          {isFiltred && (
          <>
            <Button
              variant="ghost"
              onClick={clearFilters}
              className=" px-2 lg:px-3"
            >
              Reset
              <Cross2Icon className="ml-2  w-4" />
            </Button>
            <Button
              onClick={() => navigate({ to: "/dashboard/manager/users", search: { ...filters, name: ref.current?.value ?? "" } })}
              className=" px-2 lg:px-3"
            >
              Filter
            </Button>
          </>
        )}
      </div>
    </div>
  );
}

export default UsersFilter;
