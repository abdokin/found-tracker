import Pagination from "./pagination";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { Badge } from "./ui/badge";
import UserActions from "./user-actions";
import { useQuery } from "@tanstack/react-query";
import { usersQueryOptions } from "@/routes/dashboard/manager/users";
import { useNavigate, useSearch } from "@tanstack/react-router";
import { userQueryOptions } from "@/services/user-service";

export default function ListUsers() {
  const navigate = useNavigate();
  const search = useSearch({ from: "/dashboard/manager/users" });
  const { data: current } = useQuery(userQueryOptions)
  const { data, isLoading } = useQuery(usersQueryOptions())
  if (isLoading) return <div>Loading...</div>
  if (!data) return <div>No data</div>
  return (
    <div>
      <Table className="responsive">
        <TableHeader className="bg-primary text-white">
          <TableRow>
            <TableHead>ID</TableHead>
            <TableHead>First Name</TableHead>
            <TableHead>Last Name</TableHead>
            <TableHead>Phone</TableHead>
            <TableHead>Address</TableHead>
            <TableHead>Date of Birth</TableHead>
            <TableHead>Email</TableHead>
            <TableHead>Role</TableHead>
            <TableHead>Enabled</TableHead>
            <TableHead>Created At</TableHead>
            <TableHead>Actions</TableHead>

          </TableRow>
        </TableHeader>
        <TableBody>
          {data.content.length === 0 ? (
            <TableRow>
              <TableCell colSpan={7}>
                <div className="p-16 w-full mx-auto">
                  <h1 className="text-xl mx-auto text-center">
                    No users Found
                  </h1>
                </div>
              </TableCell>
            </TableRow>
          ) : (
            data.content.map((user) => (
              <TableRow key={user.id}>
                <TableCell className="w-fit">{user.id}</TableCell>
                <TableCell>{user.firstname}</TableCell>
                <TableCell>{user.lastname}</TableCell>
                <TableCell>{user.phone}</TableCell>
                <TableCell>{user.address}</TableCell>
                <TableCell>{user.dateBirth}</TableCell>
                <TableCell>{user.email}</TableCell>
                <TableCell><Badge>{user.role}</Badge></TableCell>
                <TableCell><Badge>{user.enabled ? "Yes" : "No"}</Badge></TableCell>
                <TableCell>{user.createdAt}</TableCell>
                <TableCell>
                  {current?.id == user.id ? <div>This is your profile</div> : <UserActions user={user} />}
                </TableCell>
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>
      <div className="flex justify-between items-center p-4">
        <Pagination
          goToPage={(value: number) => {
            navigate({ to: "", search: { ...search, page: value, pageSize: data.pageable.pageSize } })

          }}
          totalPages={data.totalPages}
          pageSize={data.pageable.pageSize}
          pageNumber={data.pageable.pageNumber}
        />
        <div className="text-xs text-muted-foreground">
          Showing{" "}
          <strong>
            {data.pageable.pageNumber * data.pageable.pageSize + 1}-
            {(data.pageable.pageNumber + 1) * data.pageable.pageSize}
          </strong>{" "}
          of <strong>{data.totalElements}</strong> users
        </div>
      </div>
    </div>
  );
}
