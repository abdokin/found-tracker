import Pagination from "./pagination";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { useState } from "react";
import { Dialog, DialogContent } from "./ui/dialog";
import ReclamationsDetails from "./reclamation-details";
import { Badge } from "./ui/badge";
import { useSuspenseQuery } from "@tanstack/react-query";
import {  useSearch } from "@tanstack/react-router";
import { reclamationsQueryOptions } from "@/routes/dashboard/manager/reclamations";
import { Button } from "./ui/button";
import { Reclamation } from "@/services/reclamation-service";
import {useNavigate} from '@tanstack/react-router'
// import { userQueryOptions } from "@/lib/api";
function ListReclamations() {
  // const {data:user} = useSuspenseQuery(userQueryOptions)
  const search = useSearch({ from: "/dashboard/manager/reclamations" });
  const { data } = useSuspenseQuery(reclamationsQueryOptions())
  const navigate = useNavigate();
  const [dialogOpen, setDialogOpen] = useState(false);
  const [selectedImages, setSelectedImages] = useState<string[]>([]);

  const handleImageClick = (imageUrl: string) => {
    setSelectedImages([imageUrl]);
    setDialogOpen(true);
  };
  if (!data) {
    return null;
  }
  const { content: reclamations, totalPages, totalElements, pageable } = data;

  return (
    <div className="overflow-hidden">
      <Table>
        <TableHeader className="bg-primary text-white">
          <TableRow className="text-white">
            <TableHead className="text-white">ID</TableHead>
            <TableHead>Sujet</TableHead>
            <TableHead>Description</TableHead>
            <TableHead>Objet Name</TableHead>
            <TableHead>Objet Images</TableHead>
            <TableHead>Objet Status</TableHead>

            <TableHead>Objet Found Date Time</TableHead>
            <TableHead>Status</TableHead>
            <TableHead>Actions</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody>
          {reclamations && reclamations.length === 0 && (
            <TableRow className="">
              <TableCell colSpan={100}>
                <div className="p-16 w-full  mx-auto ">
                  <h1 className="text-xl mx-auto text-center">
                    No reclamations Found
                  </h1>
                </div>
              </TableCell>
            </TableRow>
          )}

          {reclamations?.map((reclamation: Reclamation) => (
            <TableRow key={reclamation.id}>
              <TableCell>{reclamation.code}</TableCell>
              <TableCell>{reclamation.sujet}</TableCell>
              <TableCell>{reclamation?.item.name}</TableCell>
              <TableCell>{reclamation?.item.description}</TableCell>
              <TableCell>
                <div className="w-28 overflow-x-scroll flex flex-row whitespace-nowrap rounded-md border">
                  {reclamation.item.images.map((it, index) => (
                    <button
                      key={index}
                      onClick={() => handleImageClick("/api/files/" + it.imageUrl)}
                    >
                      <img
                        src={"/api/files/" + it.imageUrl}
                        className="w-16"
                      />
                    </button>
                  ))}
                </div>
              </TableCell>
              <TableCell>
                <Badge>{reclamation?.item?.status}</Badge>
              </TableCell>
              <TableCell>
                {new Date(reclamation?.item?.foundDateTime).toDateString()}{" "}
                {new Date(
                  reclamation?.item?.foundDateTime
                ).toLocaleTimeString()}
              </TableCell>

              <TableCell><Badge>{reclamation?.status}</Badge></TableCell>
              <TableCell className="flex gap-2">
                {/* <Button variant="destructive">Delete</Button> */}
                 {/*TODO: add delete  */}
                <ReclamationsDetails reclamation={reclamation} />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div className="flex reclamations-center p-4">
        <Pagination
          goToPage={(value: number) => {
            navigate({ to: "", search: { ...search, page: value, pageSize: pageable.pageSize } })

          }}
          totalPages={totalPages}
          pageSize={pageable.pageSize}
          pageNumber={pageable.pageNumber}
        />
        <div className="text-xs text-muted-foreground">
          Showing <strong>1-{pageable.pageSize}</strong> of{" "}
          <strong>{totalElements}</strong> reclamations
        </div>
      </div>

      {dialogOpen && (
        <Dialog onOpenChange={(open) => setDialogOpen(open)} open={dialogOpen}>
          <DialogContent className="p-8 rounded-none">
            {selectedImages.map((imageUrl, index) => (
              <img key={index} src={imageUrl} className="w-full" />
            ))}
          </DialogContent>
        </Dialog>
      )}
    </div>
  );
}

export default ListReclamations;
