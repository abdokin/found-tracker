import { Button } from "@/components/ui/button";
import {
  DropdownMenuTrigger,
  DropdownMenuContent,
  DropdownMenu,
} from "@/components/ui/dropdown-menu";
import { CardTitle, CardHeader, CardContent, Card } from "@/components/ui/card";
import { IoIosNotificationsOutline } from "react-icons/io";
import { CheckIcon } from "lucide-react";
import { userQueryOptions, userService } from "@/services/user-service";
import { useQuery } from "@tanstack/react-query";
function NotificationsList() {
  const { data } = useQuery(userQueryOptions);
  const notifications = data?.notifications;
  const notificationsCount = notifications?.length ?? 0;
  const newNotificationsCount = notifications?.reduce(
    (count, notification) => (notification.opened ? count : count + 1),
    0 
  ) ?? 0;
  
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button
          size={"icon"}
          className="relative h-6 w-6 rounded-full bg-primary  bordered-none"
        >
          <IoIosNotificationsOutline size={28} className="text-white " />
          {newNotificationsCount > 0 && (
            <span className="absolute top-0 right-0 bg-red-500 text-white rounded-full px-1 py-1/2 text-xs">
              {newNotificationsCount}
            </span>
          )}
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent
        className="w-[400px] p-4 text-xs"
        align="end"
        forceMount
      >
        <Card className="shadow-none border-0">
          <CardHeader className=" p-0">
            <div className="flex items-center justify-between py-4 ">
              <CardTitle className="text-lg">Notifications</CardTitle>
              <div>
                <Button size="icon" variant="link">
                  <span className="sr-only">Mark all as read</span>
                  <CheckIcon className="w-5 h-5 text-gray-500 dark:text-gray-400" />
                </Button>
              </div>
            </div>
          </CardHeader>
          <CardContent className="px-0 flex flex-col gap-4 overflow-y-auto max-h-[400px]  ">
            {notificationsCount == 0 && <div>No Notification for the moment!</div>}
            {notifications?.map((notification, index) => (
              <div
                className="flex items-start gap-4 border px-4 py-2"
                key={index}>
                <div className="flex-1 space-y-1">
                  <div className="flex items-center justify-between">
                    <h4 className="font-medium text-sm">
                      {notification.sujet}
                    </h4>
                    <span className="text-xs text-gray-500 dark:text-gray-400">
                      {new Date(notification.receivedAt).toLocaleDateString()}
                    </span>
                  </div>
                  <p className="text-sm text-gray-500 dark:text-gray-400">
                    {notification.message}
                  </p>
                  {!notification.opened && (
                    <Button
                      size={"sm"}
                      variant={"link"}
                      className="ml-auto"
                      onClick={async () => userService.readNotification(notification.id)}
                    >read
                    </Button>
                  )}
                </div>
              </div>
            ))}
          </CardContent>
        </Card>
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

export default NotificationsList;
