import { Button } from "./ui/button";
import { Carousel, CarouselContent, CarouselItem } from "./ui/carousel";
import { Badge } from "./ui/badge";
import CreateReclamation from "./add-reclamation";
import { useMutation, useQuery } from "@tanstack/react-query";
import {
  AlertDialog,
  AlertDialogTrigger,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogCancel,
  AlertDialogAction,
} from "./ui/alert-dialog";
import { useNavigate, useSearch } from "@tanstack/react-router";
import itemService, { Item } from "@/services/item-service";
import { userQueryOptions } from "@/services/user-service";
function ItemCard({ item }: { item: Item }) {
  const { data: user } = useQuery(userQueryOptions);
  const search = useSearch({ from: "/dashboard/items" });
  const navigate = useNavigate();
  const deleteItemMutation = useMutation({
    mutationKey: ["deleteItem"],
    mutationFn: itemService.deleteItem,
    onSuccess: () => {
      navigate({ to: "/dashboard/items", search })
    }
  })

  function formatDateForHuman(dateString: string) {
    const date = new Date(dateString);
    return date.toLocaleDateString("en-US", {
      year: "numeric",
      month: "long",
      day: "numeric",
    });
  }
  return (
    <div
      className={" border w-full hover:shadow-md p-0 bg-white hover:shadow-md"}
    >
      <div className="md:flex-shrink-0 px-4 py-2w">
        <Carousel>
          <CarouselContent>
            {item.images.map((it, index) => (
              <CarouselItem key={index}>
                <img
                  className="h w-full object-cover max-h-56"
                  src={"/api/files/" + it?.imageUrl}
                  alt={item.name}
                />
              </CarouselItem>
            ))}
          </CarouselContent>
        </Carousel>
      </div>

      <div className="flex flex-col px-4 pb-2">
        <div className="py-2 flex  items-center  justify-between">
          <h5 className="text-xl text-wrap">{item.name}</h5>
          <p className="text-base font-light">
            {formatDateForHuman(item.foundDateTime)}
          </p>
        </div>
        <div className="font-light text-sm pb-4">
          {item.description.slice(0, 200)}{" "}
          <Badge className="text-xs" variant={"outline"}>
            {item.status}
          </Badge>
        </div>

        <div className="flex items-center gap-2">
          {item.status != "CLAIMED" && <CreateReclamation objetId={item.id} />}

          {user?.role === "RECEPTIONNAIRE" && (
            <AlertDialog>
              <AlertDialogTrigger asChild>
                <Button variant={"destructive"} size={"sm"}>
                  Delete
                </Button>
              </AlertDialogTrigger>
              <AlertDialogContent>
                <AlertDialogHeader>
                  <AlertDialogTitle>Are you absolutely sure?</AlertDialogTitle>
                  <AlertDialogDescription>
                    This action cannot be undone. This will permanently delete this item.
                  </AlertDialogDescription>
                </AlertDialogHeader>
                <AlertDialogFooter>
                  <AlertDialogCancel>Cancel</AlertDialogCancel>
                  <AlertDialogAction onClick={() => { deleteItemMutation.mutateAsync(item.id) }}>
                    Continue
                  </AlertDialogAction>
                </AlertDialogFooter>
              </AlertDialogContent>
            </AlertDialog>
          )}
        </div>
      </div>
    </div>
  );
}

export default ItemCard;
