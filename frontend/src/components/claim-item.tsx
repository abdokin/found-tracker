import { Button } from "./ui/button";
import { DialogContent, Dialog, DialogTrigger } from "./ui/dialog";
import { Input } from "./ui/input";
import { useState } from "react";
import { useMutation } from "@tanstack/react-query";
import { useNavigate, useSearch } from "@tanstack/react-router";
import itemService, { Item } from "@/services/item-service";
import { toast } from "sonner";
import ItemDetails from "./item-details";
export default function ClaimItem() {
  const [code, setCode] = useState("");
  const [item, setItem] = useState<Item | undefined>(undefined)
  const [open, setOpen] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(true);

  const claimItemMutation = useMutation({
    mutationKey: ["claimItem"],
    mutationFn: itemService.claimItem,
    onSuccess: (res) => {
      console.log(res);
      if (res.success) {
        // navigate({ to: "/dashboard/items", search })
        setOpen(false);
        setItem(res.data);
        setDialogOpen(true);
        setCode("");
        toast.success("Item claimed successfully")
      }
    },

  })

  return (
    <>
      {dialogOpen && item && <ItemDetails item={item} open={dialogOpen} onOpenChange={setDialogOpen} />}
      <Dialog onOpenChange={setOpen} open={open}>
        <DialogTrigger>
          <Button size={"sm"} variant={"outline"}>
            Claim
          </Button>
        </DialogTrigger>
        <DialogContent className="max-w-md w-full px-6 py-8 bg-white rounded-lg shadow-md dark:bg-gray-800">
          <div className="space-y-4">
            <div className="text-center">
              <h1 className="text-3xl font-bold text-gray-900 dark:text-gray-100">
                Reclaim Your Item
              </h1>
              <p className="text-gray-600 dark:text-gray-400">
                Enter your reclamation code to retrieve your item.
              </p>
            </div>
            <div className="space-y-4">
              <div>
                <label
                  className="block text-sm font-medium text-gray-700 dark:text-gray-300"
                  htmlFor="reclamation-code"
                >
                  Reclamation Code
                </label>
                <div className="mt-1">
                  <Input
                    onChange={(e) => setCode(e.target.value)}
                    id="reclamation-code"
                    placeholder="Enter your reclamation code"
                    type="text"
                  />
                </div>
              </div>
              <Button type="button" onClick={() => claimItemMutation.mutateAsync(code)}>
                Retrieve Item
              </Button>
            </div>
          </div>
        </DialogContent>
      </Dialog></>
  );
}

