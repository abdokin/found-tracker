import { Button } from "@/components/ui/button";
import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { useForm } from "react-hook-form";
import {
    Form,
    FormControl,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form";
import { toast } from "sonner";
import { useMutation, useQuery } from "@tanstack/react-query";
import { EditProfile, userQueryOptions, userService } from "@/services/user-service";


export default function EditProfileForm() {
    const { data: user ,refetch} = useQuery(userQueryOptions);
    if (!user) return null;
    const updateProfileMutation = useMutation({
        mutationFn: userService.updateProfile,
        
    })
    const form = useForm<EditProfile>({
        defaultValues: {
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
            phone: user.phone,
            address: user.address,
            dateOfBirth: user.dateBirth,
            cne: user.cne,
        },
    });
    async function onSubmit(values: EditProfile) {
        try {
            const res = await updateProfileMutation.mutateAsync(values);
            if (res.success) {
                toast.success(res.message);
                refetch();
            }
        } catch (res: any) {
            if (!res.success && res.errors) {
                // @ts-ignore
                Object.entries(res.errors).forEach(([field, message]) => {
                    // @ts-ignore
                    form.setError(field, {
                        message: message,
                    });
                });
                toast.error(res.message);

            }
        }
    }
    return (
        <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="">
                <Card>
                    <CardHeader>
                        <CardTitle className="text-2xl">Profile Info</CardTitle>
                        <CardDescription>
                            update your profile Information
                        </CardDescription>
                    </CardHeader>
                    <CardContent className="grid gap-4 grid-cols-2">
                        <div className="grid gap-2">
                            <FormField
                                control={form.control}
                                name="firstname"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>First Name</FormLabel>
                                        <FormControl>
                                            <Input placeholder="your first name" {...field} />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </div>
                        <div className="grid gap-2">
                            <FormField
                                control={form.control}
                                name="lastname"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>Last Name</FormLabel>
                                        <FormControl>
                                            <Input placeholder="your last name" {...field} />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </div>
                        <div className="grid gap-2">
                            <FormField
                                control={form.control}
                                name="address"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>Address</FormLabel>
                                        <FormControl>
                                            <Input placeholder="your address" {...field} />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </div>
                        <div className="grid gap-2">
                            <FormField
                                control={form.control}
                                name="phone"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>Phone</FormLabel>
                                        <FormControl>
                                            <Input placeholder="your phone" {...field} />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </div>
                        <div className="grid gap-2">
                            <FormField
                                control={form.control}
                                name="dateOfBirth"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>Date of Birth</FormLabel>
                                        <FormControl>
                                            <Input placeholder="your date of birth" {...field} type={'date'} />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </div>
                        <div className="grid gap-2">
                            <FormField
                                control={form.control}
                                name="cne"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>CNE</FormLabel>
                                        <FormControl>
                                            <Input placeholder="your CNE" {...field} />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </div>
                        <div className="grid gap-2">
                            <FormField
                                control={form.control}
                                name="email"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>Email</FormLabel>
                                        <FormControl>
                                            <Input placeholder="example@mail.com" {...field} />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                        </div>
                    </CardContent>

                    <CardFooter>
                        <Button className="w-full">Save</Button>
                    </CardFooter>
                </Card>
            </form>
        </Form>
    );
}
