import React, { useState, DragEvent, useRef, useEffect } from "react";
import { XIcon } from "lucide-react";
import { Button } from "./ui/button";
import { ControllerRenderProps } from "react-hook-form";
import { FieldValues, Path } from "react-hook-form";
export default function ImagesUpload<T extends FieldValues, N extends Path<T>>({
  field,
  addImages,
}: {
  field: ControllerRenderProps<T, N>;
  addImages: (images: File[]) => void;
}) {
  const [images, setImages] = useState<File[]>([]);
  const fileInputRef = useRef<HTMLInputElement>(null);

  const updateImage = (value: React.SetStateAction<File[]>) => {
    setImages(value);
  };

  const handleDrop = (e: DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    const files = Array.from(e.dataTransfer.files);
    const imageFiles = files.filter((file) => file.type.startsWith("image/"));
    updateImage((prevImages) => prevImages.concat(imageFiles));
  };

  const handleDragOver = (e: DragEvent<HTMLDivElement>) => {
    e.preventDefault();
  };

  const handleFileInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = Array.from(e.target.files || []);
    const imageFiles = files.filter((file) => file.type.startsWith("image/"));
    updateImage((prevImages) => prevImages.concat(imageFiles));
  };

  const handleRemoveImage = (index: number) => {
    updateImage((prevImages) => prevImages.filter((_, i) => i !== index));
  };

  const handleUploadButtonClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  useEffect(() => {
    addImages(images);
  }, [images]);

  return (
    <div>
      {images.length > 0 && (
        <div className="flex flex-wrap items-center gap-1 p-2 overflow-x-scroll">
          {images.map((image, index) => (
            <div key={index} className="relative border">
              <img
                src={URL.createObjectURL(image)}
                alt={`Image ${index}`}
                className="max-w-[100px] max-h-[100px]"
                height={100}
                width={100}
              />
              <button
                className="absolute top-0 right-0 bg-black text-white"
                type={'button'}
                onClick={() => handleRemoveImage(index)}
              >
                <XIcon size={18} />
              </button>
            </div>
          ))}
        </div>
      )}
      {images.length === 0 && (
        <div
          onDrop={handleDrop}
          onDragOver={handleDragOver}
          className="w-full h-[200px] border-2 border-dashed text-center rounded-md pt-16"
        >
          <p>Drag & Drop Images Here</p>
          <input
            type="file"
            accept="image/*"
            multiple
            style={{ display: "none" }}
            name={field.name}
            onBlur={field.onBlur}
            disabled={field.disabled}
            onChange={handleFileInputChange}
            ref={fileInputRef}
          />
          <Button type="button" onClick={handleUploadButtonClick} size="sm">
            Upload
          </Button>
        </div>
      )}
    </div>
  );
}
