import axiosInstance from "@/lib/axios";
import { ApiResponse, Image, Page } from "@/lib/types";
import { toast } from "sonner";

export type CreateItem = {
    name: string;
    description: string;
    images: File[];
}

export type Item = {
    id: number;
    name: string;
    description: string;
    foundDateTime: string;
    images: Image[];
    status: string;
};

export type ItemsFilter = {
    page: number;
    pageSize: number;
    name?: string;
    status?: string[];
    date?: Date;
};
export type EditItem = {
    name: string;
    description: string;
    images: File[];
}

export interface IItemService {
    loadItems: (filters?: ItemsFilter) => Promise<ApiResponse<Page<Item>>>;
    deleteItem: (id: number) => Promise<any>;
    createItem: (input: CreateItem) => Promise<ApiResponse<Item>>;
    editItem: (id: number, input: EditItem) => Promise<ApiResponse<Item>>;
    claimItem: (code: string) => Promise<ApiResponse<Item>>;
}
const itemService: IItemService = {

    loadItems: async (filters?: ItemsFilter) => {
        try {
            const response = await axiosInstance.get(`/items?${filters ? getEncodedItemQueryString(filters) : ""}`);
            return response.data.data;
        } catch (e) {
            return e;
        }
    },
    deleteItem: async (id: number) => {
        try {
            const response = await axiosInstance.delete(`/items/${id}`);
            return response.data;
        } catch (e) {
            return e;
        }
    },
    createItem: async (input: CreateItem) => {
        const formData = new FormData();
        formData.append("name", input.name);
        formData.append("description", input.description);
        input.images.forEach((image, index) => {
            formData.append(`images[${index}]`, image);
        });
        try {
            const response = await axiosInstance.post("/items", formData);
            return response.data;
        } catch (e) {
            return e
        }
    },
    editItem: async (id: number, input: EditItem) => {
        const formData = new FormData();
        formData.append("name", input.name);
        formData.append("description", input.description);
        input.images.forEach((image, index) => {
            formData.append(`images[${index}]`, image);
        });
        try {
            const response = await axiosInstance.put(`/items/${id}`, formData);
            return response.data;
        } catch (e) {
            return e;
        }
    },
    claimItem: async (code: string) => {
        try {
            const response = await axiosInstance.post(`/items/${code}/claim`);
            return response.data;
        } catch (e) {
            toast.error("Failed to retrieve item")
            return null
        }
    },
};

 function  getEncodedItemQueryString(filters:ItemsFilter): string {
    const params = new URLSearchParams();
  
    if (filters.name) params.append("name", filters.name);
    if (filters.date) {
      // fix toISOString not function
      params.append("date",filters.date.toISOString().split("T")[0]);
    }
    if (filters.status) {
      filters.status.forEach((status) => params.append("status[]", status));
    }
  
    params.append("page", filters.page.toString());
    params.append("pageSize", filters.pageSize.toString());
  
    return params.toString();
  }
  
export default itemService