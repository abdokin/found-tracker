import { ApiResponse, Image, Page } from "@/lib/types";
import { Item } from "./item-service";
import axiosInstance from "@/lib/axios";
import { toast } from "sonner";
import { User } from "./user-service";

export type Reclamation = {
    id: number;
    sujet: string;
    code: string;
    description: string;
    status: string;
    images: Image[];
    item: Item;
    user: User;
};
export type ReclamationFilter = {
    page: number;
    pageSize: number;
    sujet?: string;
    status?: string[];
    date?: Date;
};
export type CreateReclamation = {
    itemId: number;
    sujet: string;
    description: string;
    images: File[];
};
export interface IReclamationService {
    findReclamation: (code: string) => Promise<Reclamation | undefined>;
    acceptReclamation: (reclamationId: number) => Promise<ApiResponse<Reclamation>>;
    rejectReclamation: (reclamationId: number) => Promise<ApiResponse<Reclamation>>;
    exportReclamation: (reclamation: Reclamation) => Promise<any[]>;
    loadReclations: (filters?: ReclamationFilter) => Promise<Page<Reclamation> | undefined>;
    createReclamation: (input: CreateReclamation) => Promise<ApiResponse<Reclamation>>;
}
const reclamationService: IReclamationService = {
    findReclamation: async (code: string) => {
        try {
            const response = await axiosInstance.get(`/reclamations/${code}/track`);
            return response.data.data;
        } catch (e) {
            toast.error("Reclamation not found");
            return undefined;
        }
    },
    acceptReclamation: async (reclamationId: number) => {
        const response = await axiosInstance.post<ApiResponse<Reclamation>>(
            `/reclamations/${reclamationId}/accept`
        );
        return response.data;
    },
    rejectReclamation: async (reclamationId: number) => {
        const response = await axiosInstance.post(
            `/reclamations/${reclamationId}/reject`
        );
        return response.data;
    },
    exportReclamation: async (reclamation: Reclamation) => {
        const response = await axiosInstance.get(
            `/reclamations/${reclamation.code}/export`,
            {
                responseType: "blob"
            },
        );

        const contentDisposition = response.headers["content-disposition"];
        const fileNameMatch =
            contentDisposition && contentDisposition.match(/filename="(.+)"/);

        let fileName = reclamation.sujet + reclamation.code + ".pdf"; // Default file name
        if (fileNameMatch && fileNameMatch.length > 1) {
            fileName = fileNameMatch[1];
        }

        // Create a temporary anchor element to trigger the download
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();

        // Cleanup
        link.parentNode?.removeChild(link);
        window.URL.revokeObjectURL(url);
        return response.data;
    },
    loadReclations: async (filters?: ReclamationFilter) => {
        const response = await axiosInstance.get(`/reclamations?${filters ? getEncodedReclamationQueryString(filters) : ""}`);
        return response.data.data;
    },
    createReclamation: async (input: CreateReclamation) => {
        const formData = new FormData();
        formData.append("itemId", input.itemId.toString());
        formData.append("sujet", input.sujet);
        formData.append("description", input.description);
        input.images.forEach((image, index) => {
            formData.append(`images[${index}]`, image);
        });
        try {
            const response = await axiosInstance.post("/reclamations", formData);
            return response.data;
        } catch (e) {
            return e;
        }
    },
};
function getEncodedReclamationQueryString(filters: ReclamationFilter): string {
    const params = new URLSearchParams();

    if (filters.sujet) {
        params.append("sujet", filters.sujet);
    }
    if (filters.date)
        params.append("date", filters.date.toISOString());
    if (filters.status) {
        filters.status.forEach((status) =>
            params.append("status[]", status)
        );
    }

    params.append("page", filters.page.toString());
    params.append("pageSize", filters.pageSize.toString());

    return params.toString();
}



export default reclamationService