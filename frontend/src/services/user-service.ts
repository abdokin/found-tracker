import axiosInstance from "@/lib/axios";
import { ApiResponse, Notification, Page } from "@/lib/types";
import { queryOptions } from "@tanstack/react-query";
import { redirect } from "@tanstack/react-router";
import { toast } from "sonner";


export type Login = {
    email: string;
    password: string;
};

export type User = {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
    cne: string;
    phone: string;
    address: string;
    dateBirth: string;
    role: string;
    authorities: { authority: string }[];
    isEnabled: boolean;
    enabled: boolean;
    createdAt: string;

    notifications: Notification[];
};
export type UserFilter = {
    page: number;
    pageSize: number;
    name?: string;
    status?: string[];
    date?: Date;
};
export type ResetPassword = {
    currentPassword: string;
    newPassword: string;
    confirmationPassword: string;
};

export type EditProfile = {
    email: string;
    password: string;
    firstname: string;
    lastname: string;
    cne: string;
    phone: string;
    address: string;
    dateOfBirth: string;
};

export type RegisterInput = {
    email: string;
    password: string;
    firstname: string;
    lastname: string;
    cne: string;
    phone: string;
    address: string;
    dateOfBirth: string;
};
export type ApplicationStates = {
    totalItems: number;
    totalReclamations: number;
    totalAcceptedReclamations: number;
    totalRejectedReclamations: number;
    totalUsers: number;
    itemMonthlyCount: State[];
    reclamationMonthlyCount: State[];
};
export type State = {
    month: string;
    count: number;
};


export interface IUserService {
    getCurrentUser: () => Promise<User | null>;
    login: (input: Login) => Promise<ApiResponse<string>>;
    logout: () => Promise<any>;
    getStates: () => Promise<ApplicationStates | undefined>;
    loadUsers: (filters?: UserFilter) => Promise<Page<User>>;
    blockUser: (id: number) => Promise<ApiResponse<any>>;
    unblockUser: (id: number) => Promise<ApiResponse<any>>;
    updateProfile: (input: EditProfile) => Promise<ApiResponse<any>>;
    resetPassword: (input: ResetPassword) => Promise<ApiResponse<any>>;
    loadNotification: () => Promise<Page<Notification>>;
    readNotification: (id: number) => Promise<ApiResponse<any>>;
    register: (input: RegisterInput) => Promise<ApiResponse<any>>;
}

export const userService: IUserService = {
    getCurrentUser: async () => axiosInstance.get("/profile/current-user").then(response => response.data.data),
    login: async (input) => {
        try {
            const response = await axiosInstance.post("/auth/authenticate", input);
            return response.data;
        } catch (error) {
            // TODO : invalide;
            return error;
        }
    },
    logout: async () => {
        localStorage.removeItem("token");
    },
    getStates: async () => {
        try {
            return axiosInstance.get("/overview/monthly/" + new Date().getFullYear()).then(response => response.data.data);
        } catch (error: any) {
            toast.error(error.message);
            return null;
        }
    },
    loadUsers: async (filters) => {
        try {
            return axiosInstance.get(`/management/users?${filters ? getEncodQueryString(filters) : ""}`).then(response => response.data.data);
        } catch (e) {
            // @ts-ignore
            toast.error(e.message);
            return null;
        }
    },
    blockUser: async (id) =>  axiosInstance.post(`/management/users/${id}/block`).then(response => response.data),
    unblockUser: async (id) => {
        try {
            return axiosInstance.post(`/management/users/${id}/active`).then(response => response.data)
        } catch (e) { return e; }
    },
    updateProfile: async (input) => {
        try {
            return axiosInstance.patch("/profile/update-info", input).then(response => response.data);
        } catch (e) {
            return e;
        }
    },
    resetPassword: async (input) => {
        try {
            return axiosInstance.patch("/profile/change-password", input).then(response => response.data);
        } catch (e) {
            return e;
        }
    },
    loadNotification: async () => {
        try {
            return axiosInstance.get("/notification").then(response => response.data.data);
        } catch (e: any) {
            toast.error(e.message);
            return null;
        }
    },
    readNotification: async (id) => {
        try {
            return axiosInstance.post(`/notification/${id}`).then(response => response.data);
        } catch (e: any) {
            toast.error(e.message);
            return null;
        }
    },
    register: async (input) => {
        try {
            return axiosInstance.post("/auth/register", input).then(response => response.data);
        } catch (e) {
            return e;
        }
    }

};


export const userQueryOptions = queryOptions({
    queryKey: ["get-current-user"],
    queryFn: userService.getCurrentUser,
    staleTime: Infinity,
    retry: false,
});

function getEncodQueryString(filters: UserFilter): string {
    const params = new URLSearchParams();

    if (filters.name) params.append("name", filters.name);
    if (filters.status) {
        filters.status.forEach((status) => params.append("status[]", status));
    }
    if (filters.date) {
        // fix toISOString not function
        params.append("date", filters.date.toISOString());
    }
    params.append("page", filters.page.toString());
    params.append("pageSize", filters.pageSize.toString());

    return params.toString();
};

