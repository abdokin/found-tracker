import axios from 'axios';
const axiosInstance = axios.create({
    baseURL: "/api",
});

axiosInstance.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem('token');
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

// Response interceptor to handle responses and errors globally
axiosInstance.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        // if (error.response && error.response.status === 403) {
        //     // localStorage.removeItem("token"); // TODO:Remove
        // }
        return Promise.reject(error.response.data);
    }
);

export default axiosInstance;
