export type ApiResponse<T> = {
  success: boolean;
  message: string;
  // error must be array[filed => value]
  errors?: Record<string, string>;
  data?: T;
}


export interface ErrorResponse {
  success: boolean;
  message: string;
  timestamp: string;
  errors: {
    field: string;
    message: string;
  }[];
}

export type Notification = {
  id: number;
  message: string;
  sujet: string;
  opened: boolean;
  receivedAt: string;
};

export type Pagination = {
  pageNumber: number;
  pageSize: number;
};


export type Pageable = {
  pageNumber: number;
  pageSize: number;
  sort: {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
  };
  offset: number;
  paged: boolean;
  unpaged: boolean;
};

export type Page<T> = {
  content: T[];
  pageable: Pageable;
  totalPages: number;
  totalElements: number;
  last: boolean;
  size: number;
  number: number;
  sort: {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
  };
  numberOfElements: number;
  first: boolean;
  empty: boolean;
};


export type Image = {
  id: number;
  imageUrl: string;
  imageName: string;
};

