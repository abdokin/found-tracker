import axiosInstance from "@/lib/axios";
import { AddItemInput, ApplicationStates, AuthResponse, CreateReclamationInput, EditItemInput, EditProfileInput, ErrorResponse, Item, ItemsFilter, LoginInput, Page, Reclamation, ReclamationFilter, ResetPasswordInput, User, UserFilter } from "@/lib/types";
import { queryOptions } from "@tanstack/react-query";
import { redirect } from "@tanstack/react-router";
import axios from "axios";
import { toast } from "sonner";
import { getEncodedItemQueryString, getEncodedReclamationQueryString, getEncodedUserQueryString } from "./utils";

async function getCurrentUser() {
  const response = await axiosInstance.get<User>("/profile/current-user");
  if (response.status !== 200) {
    throw new Error("Failed to fetch user data");
  }
  if (!response.data) {
    throw new Error("User not found");
  }
  return response.data;
}
export async function login(
  input: LoginInput
): Promise<{ success: boolean; error?: ErrorResponse, data?: User }> {
  try {
    const response = await axiosInstance.post<AuthResponse>("/auth/authenticate", input);
    const data = response.data;
    localStorage.setItem("token", data.token);
    return { success: true, data: response.data.user };
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      const errorResponse: ErrorResponse = error.response.data;
      return { success: false, error: errorResponse };
    } else {
      const errorResponse: ErrorResponse = {
        success: false,
        message: "Network error or server is down",
        timestamp: new Date().toISOString(),
        errors: [],
      };
      return { success: false, error: errorResponse };
    }
  }
}
export async function logout() {
  localStorage.removeItem("token");
  throw redirect({ to: "/" });
}

async function getItemFilters(): Promise<ItemsFilter> {
  const filters = localStorage.getItem("items-filters");
  return filters ? JSON.parse(filters) : {};
}
export const itemFilterQueryOptions = queryOptions({
  queryKey: ["item-filters"],
  queryFn: getItemFilters,
});


export async function loadItems(filters?: ItemsFilter) {
  console.log("load items", filters);
  const response = await axiosInstance.get<Page<Item>>(
    `/items?${filters && getEncodedItemQueryString(filters)}`
  );
  if (response.status !== 200) {
    throw new Error("Failed to load items");
  }
  return response.data;

}
// find reclamation 
export async function findReclamation(code: string) {
  const response = await axiosInstance.get<Reclamation>(`/reclamations/track/${code}`);
  if (response.status !== 200) {
    throw new Error("Reclamation not found");
  }
  return response.data;

}
// accept reclamation
export async function acceptReclamation(reclamationId: number) {
  try {
    const response = await axiosInstance.post<Reclamation>(
      `/reclamations/${reclamationId}/accept`
    );
    if (response.status !== 200) {
      throw new Error("Failed to accept reclamation");
    }
    return { success: true };
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      const errorResponse: ErrorResponse = error.response.data;
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    } else {
      const errorResponse: ErrorResponse = {
        success: false,
        message: "Network error or server is down",
        timestamp: new Date().toISOString(),
        errors: [],
      };
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    }
  }
}
// reject reclamation
export async function rejectReclamation(reclamationId: number) {
  try {
    const response = await axiosInstance.post<Reclamation>(
      `/reclamations/${reclamationId}/reject`
    );
    if (response.status !== 200) {
      throw new Error("Failed to reject reclamation");
    }
    return { success: true };
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      const errorResponse: ErrorResponse = error.response.data;
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    } else {
      const errorResponse: ErrorResponse = {
        success: false,
        message: "Network error or server is down",
        timestamp: new Date().toISOString(),
        errors: [],
      };
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    }
  }
}
// export reclamation
export async function exportReclamation(reclamation: Reclamation) {
  try {
    const response = await axiosInstance.post(
      `/reclamations/export/${reclamation.code}`,
      { responseType: "blob" }

    );
    if (response.status !== 200) {
      throw new Error("Failed to export reclamation");
    }
    
    const contentDisposition = response.headers["content-disposition"];
    const fileNameMatch =
      contentDisposition && contentDisposition.match(/filename="(.+)"/);

    let fileName = reclamation.sujet + reclamation.code + ".pdf"; // Default file name
    if (fileNameMatch && fileNameMatch.length > 1) {
      fileName = fileNameMatch[1];
    }

    // Create a temporary anchor element to trigger the download
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", fileName);
    document.body.appendChild(link);
    link.click();

    // Cleanup
    link.parentNode?.removeChild(link);
    window.URL.revokeObjectURL(url);
    return { success: true };
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      const errorResponse: ErrorResponse = error.response.data;
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    } else {
      const errorResponse: ErrorResponse = {
        success: false,
        message: "Network error or server is down",
        timestamp: new Date().toISOString(),
        errors: [],
      };
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    }
  }
}

export async function loadReclations(filters?: ReclamationFilter) {
  try {
    const response = await axiosInstance.get<Page<Reclamation>>(
      `/reclamations?${filters ? getEncodedReclamationQueryString(filters) : ""}`
    );
    if (response.status !== 200) {
      throw new Error("Failed to load reclamations");
    }
    return response.data;
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      const errorResponse: ErrorResponse = error.response.data;
      toast.error(errorResponse.message);
    } else {
      const errorResponse: ErrorResponse = {
        success: false,
        message: "Network error or server is down",
        timestamp: new Date().toISOString(),
        errors: [],
      };
      toast.error(errorResponse.message);
    }
  }
}

export async function createItem(input: AddItemInput) {
  try {
    const formData = new FormData();
    formData.append("name", input.name);
    formData.append("description", input.description);
    input.images.forEach((image, index) => {
      formData.append(`images[${index}]`, image);
    });

    const response = await axiosInstance.post<Item>("/items", formData);
    if (response.status !== 200) {
      throw new Error("Failed to create item");
    }
    return { success: true, data: response.data };
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      const errorResponse: ErrorResponse = error.response.data;
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    } else {
      const errorResponse: ErrorResponse = {
        success: false,
        message: "Network error or server is down",
        timestamp: new Date().toISOString(),
        errors: [],
      };
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    }
  }
}
export async function createReclamation(input: CreateReclamationInput) {
  try {
    const formData = new FormData();
    formData.append("subject", input.sujet);
    formData.append("description", input.description);
    input.images.forEach((image, index) => {
      formData.append(`images[${index}]`, image);
    });

    const response = await axiosInstance.post<String>("/reclamations", formData);
    if (response.status !== 200) {
      throw new Error("Failed to create item");
    }
    return { success: true, data: response.data };
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      const errorResponse: ErrorResponse = error.response.data;
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    } else {
      const errorResponse: ErrorResponse = {
        success: false,
        message: "Network error or server is down",
        timestamp: new Date().toISOString(),
        errors: [],
      };
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    }
  }
}


// delete item function
export async function deleteItem(id: number) {
  const response = await axiosInstance.delete(`/items/${id}/delete`);
  if (response.status !== 200) {
    throw new Error("Failed to delete item");
  }
  return response.data;
}

// edit item function
export async function editItem(id: number, input: EditItemInput) {
  try {
    const formData = new FormData();
    formData.append("name", input.name);
    formData.append("description", input.description);
    input.images.forEach((image, index) => {
      formData.append(`images[${index}]`, image);
    });
    const response = await axiosInstance.put(`/items/${id}`, formData);
    if (response.status !== 200) {
      throw new Error("Failed to edit item");
    }
    return { success: true, data: response.data };
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      const errorResponse: ErrorResponse = error.response.data;
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    } else {
      const errorResponse: ErrorResponse = {
        success: false,
        message: "Network error or server is down",
        timestamp: new Date().toISOString(),
        errors: [],
      };
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    }
  }
}

export async function claimItem(code: string): Promise<{ success: boolean }> {
  try {
    const response = await axiosInstance.post<Reclamation>(
      `/reclamations/claim/${code}`
    );
    if (response.status !== 200) {
      throw new Error("Failed to claim item");
    }
    toast.success("Item Claimed");
    return { success: true };
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      const errorResponse: ErrorResponse = error.response.data;
      toast.error(errorResponse.message);
      return { success: false };
    } else {
      const errorResponse: ErrorResponse = {
        success: false,
        message: "Network error or server is down",
        timestamp: new Date().toISOString(),
        errors: [],
      };
      toast.error(errorResponse.message);
      return { success: false };
    }
  }
}


export async function getStates() {
  try {
    const year = new Date().getFullYear();
    const response = await axiosInstance.get<ApplicationStates>("/overview/monthly/" + year);
    return response.data;
  } catch (error) {
    console.error("Failed to fetch monthly counts:", error);
  }
}

export async function loadUsers(filters?: UserFilter) {
  const response = await axiosInstance.get<Page<User>>(
    `management/users?${filters ? getEncodedUserQueryString(filters) : ""}`
  );
  if (response.status !== 200) {
    throw new Error("Failed to load users");
  }
  return response.data;
}

export async function blockUser(id: number) {
  const response = await axiosInstance.post(`/management/users/block/${id}`);
  if (response.status !== 200) {
    throw new Error("Failed to block user");
  }
  return response.data;
}
// unblock user function
export async function unblockUser(id: number) {
  const response = await axiosInstance.post(`/management/users/active/${id}`);
  if (response.status !== 200) {
    throw new Error("Failed to unblock user");
  }
  return response.data;
}


// update profile  json
export async function updateProfile(input: EditProfileInput) {
  try {
    const response = await axiosInstance.patch("/profile/update-info", input);
    if (response.status !== 200) {
      throw new Error("Failed to update profile");
    }
    return response.data;
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      const errorResponse: ErrorResponse = error.response.data;
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    } else {
      const errorResponse: ErrorResponse = {
        success: false,
        message: "Network error or server is down",
        timestamp: new Date().toISOString(),
        errors: [],
      };
      toast.error(errorResponse.message);
      return { success: false, error: errorResponse };
    }
  }
}
// reset password
export async function resetPassword(input: ResetPasswordInput) {
  const response = await axiosInstance.patch("/profile/change-password", input);
  if (response.status !== 200) {
    throw new Error("Failed to reset password");
  }
  return response.data;
}
